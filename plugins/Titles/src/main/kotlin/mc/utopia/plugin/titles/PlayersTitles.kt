package mc.utopia.plugin.titles

import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import org.bukkit.Bukkit.getScoreboardManager
import org.bukkit.entity.Player
import java.io.File

class PlayersTitles(
    private val directory: File,
    private val currentTitlesDirectory: File,
    private val defaultTitle: String
) {
    operator fun get(player: Player): Set<String> = getMutable(player)

    fun has(player: Player, title: String): Boolean {
        return title in get(player)
    }

    fun add(player: Player, title: String) {
        val playerTitles = getMutable(player)
        playerTitles += title
        save(player, playerTitles)
    }

    fun remove(player: Player, title: String) {
        val playerTitles = getMutable(player)
        playerTitles -= title
        save(player, playerTitles)
        if (getCurrent(player) == title) {
            removeCurrent(player)
        }
    }

    private fun save(player: Player, titles: Set<String>) {
        directory.mkdirs()
        val file = directory.resolve("${player.uniqueId}.json")
        file.writer().use {
            val gson = Gson()
            gson.toJson(titles, it)
        }
    }

    private fun getMutable(player: Player): MutableSet<String> {
        val file = directory.resolve("${player.uniqueId}.json")
        if (!file.exists()) return mutableSetOf()
        return file.reader().use {
            val gson = Gson()
            gson.fromJson(it, object : TypeToken<MutableSet<String>>() {}.type)
        }
    }

    fun getCurrent(player: Player): String {
        val file = currentTitlesDirectory.resolve("${player.uniqueId}.txt")
        if (!file.exists()) return get(player).firstOrNull()?.also {
            setCurrent(player, it)
        } ?: defaultTitle
        return file.readText()
    }

    fun setCurrent(player: Player, title: String) {
        currentTitlesDirectory.mkdirs()
        val file = currentTitlesDirectory.resolve("${player.uniqueId}.txt")
        file.writeText(title)
        getScoreboardManager().mainScoreboard.getTeam(title)?.addEntry(player.name)
    }

    fun removeCurrent(player: Player) {
        val file = currentTitlesDirectory.resolve("${player.uniqueId}.txt")
        file.delete()
    }
}

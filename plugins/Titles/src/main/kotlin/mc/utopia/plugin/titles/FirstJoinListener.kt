package mc.utopia.plugin.titles

import org.bukkit.Bukkit.getScoreboardManager
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class FirstJoinListener(private val defaultTitle: String) : Listener {
    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val player = event.player
        if (!player.hasPlayedBefore()) {
            getScoreboardManager().mainScoreboard.getTeam(defaultTitle)?.addEntry(player.name)
        }
    }
}
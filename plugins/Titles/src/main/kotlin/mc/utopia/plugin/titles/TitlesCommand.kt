package mc.utopia.plugin.titles

import mc.utopia.plugin.util.*
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

fun titlesCommand(
    titles: Map<String, Text>,
    playersTitles: PlayersTitles,
    strings: TitlesCommandStrings
): TreeCommand {
    val subCommands = mapOf(
        "list" to TitlesListCommand(titles, playersTitles, strings.list),
        "set" to TitlesSetCommand(titles, playersTitles, strings.set),
        "add" to AdminCommand(TitlesAddCommand(titles, playersTitles, strings.add), strings.usage),
        "remove" to AdminCommand(TitlesRemoveCommand(playersTitles, strings.remove), strings.usage)
    )
    return TreeCommand(subCommands, strings.usage)
}

data class TitlesCommandStrings(
    val usage: Text = emptyList(),
    val list: TitlesListCommand.Strings = TitlesListCommand.Strings(),
    val set: TitlesSetCommand.Strings = TitlesSetCommand.Strings(),
    val add: TitlesAddCommand.Strings = TitlesAddCommand.Strings(),
    val remove: TitlesRemoveCommand.Strings = TitlesRemoveCommand.Strings()
)

class TitlesListCommand(
    private val titles: Map<String, Text>,
    private val playersTitles: PlayersTitles,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        val currentTitle = titles[playersTitles.getCurrent(sender)] ?: error("cannot find title ${playersTitles.getCurrent(sender)}")
        val titles = playersTitles[sender].map { titles[it] ?: error("cannot find title $it") }
        if (titles.isNotEmpty()) {
            sender.sendMessage(strings.current + currentTitle)
            sender.sendMessage(strings.all + titles.reduce { text1, text2 -> text1 + strings.separator + text2 })
        } else {
            sender.sendMessage(strings.empty)
        }
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    data class Strings(
        val usage: Text = emptyList(),
        val current: Text = emptyList(),
        val all: Text = emptyList(),
        val separator: Text = emptyList(),
        val empty: Text = emptyList()
    )
}

class TitlesSetCommand(
    private val titles: Map<String, Text>,
    private val playersTitles: PlayersTitles,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.isEmpty()) {
            sender.sendMessage(strings.usage)
            return false
        }
        val title = args.joinToString(" ")
        return if (playersTitles.has(sender, title)) {
            playersTitles.setCurrent(sender, title)
            sender.sendMessage(strings.currentTitleSet)
            true
        } else {
            sender.sendMessage(strings.missingTitle, mapOf("title" to title))
            false
        }
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return if (args.size == 1) {
            playersTitles[sender].toList()
        } else {
            emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val currentTitleSet: Text = emptyList(),
        val missingTitle: Text = emptyList()
    )
}

class TitlesAddCommand(
    private val titles: Map<String, Text>,
    private val playersTitles: PlayersTitles,
    private val strings: Strings
) : ConsoleCommand {
    override fun onCommand(sender: CommandSender, name: String, vararg args: String): Boolean {
        if (args.size < 2) {
            sender.sendMessage(strings.usage)
            return false
        }
        val playerName = args[0]
        val player = Bukkit.getPlayer(playerName)
        if (player == null) {
            sender.sendMessage(strings.playerNotFound)
            return false
        }
        val titleName = args[1]
        if (titleName in playersTitles[player]) {
            sender.sendMessage(strings.titleAlreadyGet)
            return false
        }
        if (titleName !in titles) {
            sender.sendMessage(strings.titleNotFound)
            return false
        }
        playersTitles.add(player, titleName)
        sender.sendMessage(strings.titleAdded)
        return true
    }

    override fun onTabComplete(sender: CommandSender, name: String, vararg args: String): List<String> {
        return when (args.size) {
            1 -> onlinePlayers.map { it.name }
            2 -> {
                val player = Bukkit.getPlayer(args[0])
                if (player != null) (titles.keys - playersTitles[player]).toList() else emptyList()
            }
            else -> emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val playerNotFound: Text = emptyList(),
        val titleAlreadyGet: Text = emptyList(),
        val titleNotFound: Text = emptyList(),
        val titleAdded: Text = emptyList()
    )
}

class TitlesRemoveCommand(
    private val playersTitles: PlayersTitles,
    private val strings: Strings
) : ConsoleCommand {
    override fun onCommand(sender: CommandSender, name: String, vararg args: String): Boolean {
        if (args.size < 2) {
            sender.sendMessage(strings.usage)
            return false
        }
        val playerName = args[0]
        val player = Bukkit.getPlayer(playerName)
        if (player == null) {
            sender.sendMessage(strings.playerNotFound)
            return false
        }
        val titleName = args[1]
        if (titleName !in playersTitles[player]) {
            sender.sendMessage(strings.titleNotGet)
            return false
        }
        playersTitles.remove(player, titleName)
        sender.sendMessage(strings.titleRemoved)
        return true
    }

    override fun onTabComplete(sender: CommandSender, name: String, vararg args: String): List<String> {
        return when (args.size) {
            1 -> onlinePlayers.map { it.name }
            2 -> {
                val player = Bukkit.getPlayer(args[0])
                if (player != null) playersTitles[player].toList() else emptyList()
            }
            else -> emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val playerNotFound: Text = emptyList(),
        val titleNotGet: Text = emptyList(),
        val titleRemoved: Text = emptyList()
    )
}
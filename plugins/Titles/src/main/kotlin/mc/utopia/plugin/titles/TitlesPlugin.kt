package mc.utopia.plugin.titles

import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.registerCommand
import mc.utopia.plugin.util.registerEvents
import net.md_5.bungee.api.chat.BaseComponent
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin

class TitlesPlugin : JavaPlugin() {
    private val config: Config
    private val playersTitles: PlayersTitles
    val titles: Map<String, Text>

    init {
        config = fromJsonOrElse("config.json", Config())
        titles = config.titles
        val (playersTitlesPath, playersCurrentTitlesPath, defaultTitle) = config
        playersTitles = PlayersTitles(dataFolder.resolve(playersTitlesPath), dataFolder.resolve(playersCurrentTitlesPath), defaultTitle)
    }

    override fun onEnable() {
        registerEvents(FirstJoinListener(config.defaultTitle))
        registerCommand("titles", titlesCommand(config.titles, playersTitles, config.commands.titles))
        val scoreboard = Bukkit.getScoreboardManager()!!.mainScoreboard
        config.titles.forEach { (name, title) ->
            if (scoreboard.getTeam(name) == null) {
                scoreboard.registerNewTeam(name)
            }
            scoreboard.getTeam(name)?.apply {
                displayName = BaseComponent.toLegacyText(*title.toTypedArray())
                prefix = "$displayName "
            }
        }
    }

    fun getTitles(player: Player) = playersTitles[player]
    fun hasTitle(player: Player, title: String) = playersTitles.has(player, title)
    fun addTitle(player: Player, title: String) = playersTitles.add(player, title)
    fun removeTitle(player: Player, title: String) = playersTitles.remove(player, title)
    fun getCurrentTitle(player: Player) = playersTitles.getCurrent(player)
    fun setCurrentTitle(player: Player, title: String) {
        playersTitles.setCurrent(player, title)
    }

    data class Config(
        val playersTitlesPath: String = "titles",
        val playersCurrentTitlesPath: String = "titles/current",
        val defaultTitle: String = "unknown",
        val titles: Map<String, Text> = emptyMap(),
        val commands: Commands = Commands()
    ) {
        data class Commands(
            val titles: TitlesCommandStrings = TitlesCommandStrings()
        )
    }
}
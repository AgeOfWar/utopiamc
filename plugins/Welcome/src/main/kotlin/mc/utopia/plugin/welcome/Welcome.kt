package mc.utopia.plugin.welcome

import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.runTask
import mc.utopia.plugin.util.sendMessage
import org.bukkit.Bukkit.getAdvancement
import org.bukkit.NamespacedKey
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class Welcome(
        private val plugin: WelcomePlugin,
        private val welcomeMessage: Text?
) : Listener {
    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val player = event.player
        val firstAdvancement = getAdvancement(NamespacedKey("advancements", "adventure/go_to_world"))
            ?: error("cannot find advancement adventure/go_to_world")
        if (!player.getAdvancementProgress(firstAdvancement).isDone) {
            welcomeMessage?.let {
                plugin.runTask {
                    player.sendMessage(welcomeMessage)
                }
            }
        }
    }
}
package mc.utopia.plugin.welcome

import mc.utopia.plugin.util.*
import org.bukkit.plugin.java.JavaPlugin

class WelcomePlugin : JavaPlugin() {
    private val config = fromJsonOrElse("config.json", Config())

    override fun onEnable() {
        registerEvents(Welcome(this, config.welcomeMessage))
    }

    data class Config(
        val welcomeMessage: Text? = emptyList()
    )
}

package mc.utopia.plugin.particles

import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerQuitEvent
import java.io.File

class PlayersParticles(private val directory: File, private val currentParticlesDirectory: File) : Listener {
    private val playerCurrentParticles = mutableMapOf<Player, String?>()

    operator fun get(player: Player): Set<String> = getMutable(player)

    fun has(player: Player, particles: String): Boolean {
        return particles in get(player)
    }

    fun add(player: Player, particles: String) {
        val playerParticles = getMutable(player)
        playerParticles += particles
        save(player, playerParticles)
    }

    fun remove(player: Player, particles: String) {
        val playerTrails = getMutable(player)
        playerTrails -= particles
        save(player, playerTrails)
        if (getCurrent(player) == particles) removeCurrent(player)
    }

    private fun save(player: Player, particles: Set<String>) {
        directory.mkdirs()
        val file = directory.resolve("${player.uniqueId}.json")
        file.writer().use {
            val gson = Gson()
            gson.toJson(particles, it)
        }
    }

    private fun getMutable(player: Player): MutableSet<String> {
        val file = directory.resolve("${player.uniqueId}.json")
        if (!file.exists()) return mutableSetOf()
        return file.reader().use {
            val gson = Gson()
            gson.fromJson(it, object : TypeToken<MutableSet<String>>() {}.type)
        }
    }

    fun getCurrent(player: Player): String? {
        return if (player in playerCurrentParticles) {
            playerCurrentParticles[player]
        } else {
            val file = currentParticlesDirectory.resolve("${player.uniqueId}.txt")
            if (!file.exists()) return null
            file.readText()
        }
    }

    fun setCurrent(player: Player, particles: String) {
        playerCurrentParticles[player] = particles
        currentParticlesDirectory.mkdirs()
        val file = currentParticlesDirectory.resolve("${player.uniqueId}.txt")
        file.writeText(particles)
    }

    fun removeCurrent(player: Player) {
        playerCurrentParticles[player] = null
        val file = currentParticlesDirectory.resolve("${player.uniqueId}.txt")
        file.delete()
    }

    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        playerCurrentParticles.remove(event.player)
    }
}

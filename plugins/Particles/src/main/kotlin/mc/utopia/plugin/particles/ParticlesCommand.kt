package mc.utopia.plugin.particles

import mc.utopia.plugin.util.*
import org.bukkit.Bukkit
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

fun particlesCommand(
    strings: ParticlesCommandStrings,
    particles: Map<String, Particles>,
    playersParticles: PlayersParticles
): TreeCommand {
    val subCommands = mapOf(
        "list" to ParticlesListCommand(playersParticles, strings.noneParticleName, strings.list),
        "set" to ParticlesSetCommand(playersParticles, strings.noneParticleName, strings.set),
        "add" to AdminCommand(ParticlesAddCommand(particles, playersParticles, strings.add) ,strings.usage),
        "remove" to AdminCommand(ParticlesRemoveCommand(playersParticles, strings.remove) ,strings.usage)
    )
    return TreeCommand(subCommands, strings.usage)
}

data class ParticlesCommandStrings(
    val usage: Text = emptyList(),
    val set: ParticlesSetCommand.Strings = ParticlesSetCommand.Strings(),
    val list: ParticlesListCommand.Strings = ParticlesListCommand.Strings(),
    val add: ParticlesAddCommand.Strings = ParticlesAddCommand.Strings(),
    val remove: ParticlesRemoveCommand.Strings = ParticlesRemoveCommand.Strings(),
    val noneParticleName: String = "none"
)

class ParticlesSetCommand(
    private val playersParticles: PlayersParticles,
    private val noneParticleName: String,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.isEmpty()) {
            sender.sendMessage(strings.usage)
            return false
        }
        val particle = args.joinToString(" ")
        return when {
            particle == noneParticleName -> {
                playersParticles.removeCurrent(sender)
                sender.sendMessage(strings.currentParticleSet)
                true
            }
            playersParticles.has(sender, particle) -> {
                playersParticles.setCurrent(sender, particle)
                sender.sendMessage(strings.currentParticleSet)
                true
            }
            else -> {
                sender.sendMessage(strings.missingParticle, mapOf("particle" to particle))
                false
            }
        }
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return if (args.size == 1) {
            playersParticles[sender].toList() + noneParticleName
        } else {
            emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val currentParticleSet: Text = emptyList(),
        val missingParticle: Text = emptyList()
    )
}

class ParticlesListCommand(
    private val playersParticles: PlayersParticles,
    private val noneParticleName: String,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        val particles = playersParticles[sender]
        if (particles.isNotEmpty()) {
            sender.sendMessage(
                strings.info, mapOf(
                    "particles" to particles.joinToString(", "),
                    "currentParticles" to (playersParticles.getCurrent(sender) ?: noneParticleName)
                )
            )
        } else {
            sender.sendMessage(strings.empty)
        }
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    data class Strings(
        val usage: Text = emptyList(),
        val info: Text = emptyList(),
        val empty: Text = emptyList()
    )
}

class ParticlesAddCommand(
    private val particles: Map<String, Particles>,
    private val playersParticles: PlayersParticles,
    private val strings: Strings
) : ConsoleCommand {
    override fun onCommand(sender: CommandSender, name: String, vararg args: String): Boolean {
        if (args.size < 2) {
            sender.sendMessage(strings.usage)
            return false
        }
        val playerName = args[0]
        val player = Bukkit.getPlayer(playerName)
        if (player == null) {
            sender.sendMessage(strings.playerNotFound)
            return false
        }
        val particleName = args[1]
        if (particleName in playersParticles[player]) {
            sender.sendMessage(strings.particleAlreadyGet)
            return false
        }
        if (particleName !in particles) {
            sender.sendMessage(strings.particleNotFound)
            return false
        }
        playersParticles.add(player, particleName)
        sender.sendMessage(strings.particleAdded)
        return true
    }

    override fun onTabComplete(sender: CommandSender, name: String, vararg args: String): List<String> {
        return when (args.size) {
            1 -> onlinePlayers.map { it.name }
            2 -> {
                val player = Bukkit.getPlayer(args[0])
                if (player != null) (particles.keys - playersParticles[player]).toList() else emptyList()
            }
            else -> emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val playerNotFound: Text = emptyList(),
        val particleAlreadyGet: Text = emptyList(),
        val particleNotFound: Text = emptyList(),
        val particleAdded: Text = emptyList()
    )
}

class ParticlesRemoveCommand(
    private val playersParticles: PlayersParticles,
    private val strings: Strings
) : ConsoleCommand {
    override fun onCommand(sender: CommandSender, name: String, vararg args: String): Boolean {
        if (args.size < 2) {
            sender.sendMessage(strings.usage)
            return false
        }
        val playerName = args[0]
        val player = Bukkit.getPlayer(playerName)
        if (player == null) {
            sender.sendMessage(strings.playerNotFound)
            return false
        }
        val particleName = args[1]
        if (particleName !in playersParticles[player]) {
            sender.sendMessage(strings.particleNotGet)
            return false
        }
        playersParticles.remove(player, particleName)
        sender.sendMessage(strings.particleRemoved)
        return true
    }

    override fun onTabComplete(sender: CommandSender, name: String, vararg args: String): List<String> {
        return when (args.size) {
            1 -> onlinePlayers.map { it.name }
            2 -> {
                val player = Bukkit.getPlayer(args[0])
                if (player != null) playersParticles[player].toList() else emptyList()
            }
            else -> emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val playerNotFound: Text = emptyList(),
        val particleNotGet: Text = emptyList(),
        val particleRemoved: Text = emptyList()
    )
}

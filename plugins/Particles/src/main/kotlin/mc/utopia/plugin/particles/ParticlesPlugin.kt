package mc.utopia.plugin.particles

import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.onlinePlayers
import mc.utopia.plugin.util.registerCommand
import mc.utopia.plugin.util.registerEvents
import org.bukkit.Bukkit.getScheduler
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitTask

class ParticlesPlugin : JavaPlugin() {
    private val config = fromJsonOrElse("config.json", Config())
    private val playersParticles = PlayersParticles(dataFolder.resolve(config.playerParticlesPath), dataFolder.resolve(config.playersCurrentParticlesPath))

    private val tasks = mutableMapOf<Player, List<BukkitTask>>()

    val particles: Map<String, Particles> = loadParticles(dataFolder.resolve(config.particlesPath))

    override fun onEnable() {
        val particleStarter = ParticleStarter(this)
        registerEvents(particleStarter)
        registerEvents(playersParticles)
        onlinePlayers.forEach { particleStarter.addPlayer(it) }
        registerCommand(
            "particles",
            particlesCommand(config.particles, particles, playersParticles)
        )
    }

    fun getParticles(player: Player) = playersParticles[player]
    fun hasParticle(player: Player, particles: String) = playersParticles.has(player, particles)
    fun addParticle(player: Player, particles: String) = playersParticles.add(player, particles)
    fun removeParticles(player: Player, particles: String) = playersParticles.remove(player, particles)
    fun getCurrentParticles(player: Player) = playersParticles.getCurrent(player)
    fun setCurrentParticles(player: Player, particles: String) {
        playersParticles.setCurrent(player, particles)
    }
    fun removeCurrentParticles(player: Player) {
        playersParticles.removeCurrent(player)
    }

    fun startParticles(player: Player, particles: Particles?) {
        tasks[player]?.forEach { it.cancel() }
        tasks.remove(player)
        val task = particles?.elements?.map { element ->
            var t = 0L
            val (particleType, x, y, z, offsetX, offsetY, offsetZ, function, offsetFunction, count, delay, period, extra) = element
            getScheduler().runTaskTimer(this, { ->
                val actualCoordinates = function(t)
                val actualOffset = offsetFunction(t)
                actualCoordinates.zip(actualOffset).forEach { (coordinates, offset) ->
                    val location = player.location
                    val actualX = location.x + coordinates.x + x
                    val actualY = location.y + coordinates.y + y
                    val actualZ = location.z + coordinates.z + z
                    val actualOffsetX = offset.x + offsetX
                    val actualOffsetY = offset.y + offsetY
                    val actualOffsetZ = offset.z + offsetZ
                    player.world.spawnParticle(
                        particleType.value,
                        actualX,
                        actualY,
                        actualZ,
                        count,
                        actualOffsetX,
                        actualOffsetY,
                        actualOffsetZ,
                        extra,
                        particleType.data,
                        true
                    )
                }
                t += delay
            }, delay, period)
        }
        if (task != null) tasks[player] = task
    }

    data class Config(
        val particlesPath: String = "particles",
        val playerParticlesPath: String = "particles/players",
        val playersCurrentParticlesPath: String = "particles/players/current",
        val particles: ParticlesCommandStrings = ParticlesCommandStrings()
    )
}
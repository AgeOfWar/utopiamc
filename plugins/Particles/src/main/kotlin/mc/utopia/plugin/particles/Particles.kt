package mc.utopia.plugin.particles

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.annotations.JsonAdapter
import mc.utopia.plugin.util.gson
import java.io.File
import org.bukkit.Particle as BukkitParticle

data class Particles(val elements: List<Element>) {
    data class Type(val value: BukkitParticle, val data: Any?)

    @JsonAdapter(Element.Serializer::class)
    data class Element(
        val particleType: Type,
        val x: Double = 0.0,
        val y: Double = 0.0,
        val z: Double = 0.0,
        val offsetX: Double = 0.0,
        val offsetY: Double = 0.0,
        val offsetZ: Double = 0.0,
        val function: (Long) -> Sequence<Coordinates> = { _ -> sequenceOf(Coordinates(0.0, 0.0, 0.0)) },
        val offsetFunction: (Long) -> Sequence<Coordinates> = { _ -> sequenceOf(Coordinates(0.0, 0.0, 0.0)) },
        val count: Int = 1,
        val delay: Long = 2L,
        val period: Long = 1L,
        val extra: Double = 0.0
    ) {
        class Serializer : JsonDeserializer<Element> {
            override fun deserialize(json: JsonElement, typeOfT: java.lang.reflect.Type, context: JsonDeserializationContext): Element? {
                json as JsonObject
                val type = BukkitParticle.valueOf(json["type"].asString.toUpperCase())
                val data = if (json.has("data")) context.deserialize<Any?>(json["data"], type.dataType) else null
                val particleType = Type(type, data)
                val x = if (json.has("x")) json["x"].asDouble else 0.0
                val y = if (json.has("y")) json["y"].asDouble else 0.0
                val z = if (json.has("z")) json["z"].asDouble else 0.0
                val offsetX = if (json.has("offset_x")) json["offset_x"].asDouble else 0.0
                val offsetY = if (json.has("offset_y")) json["offset_y"].asDouble else 0.0
                val offsetZ = if (json.has("offset_z")) json["offset_z"].asDouble else 0.0
                val count = if (json.has("count")) json["count"].asInt else 1
                val delay = if (json.has("delay")) json["delay"].asLong else 2L
                val period = if (json.has("period")) json["period"].asLong else 0L
                val extra = if (json.has("extra")) json["extra"].asDouble else 0.0
                val parameters = mutableMapOf<String, Any?>()
                val offsetParameters = mutableMapOf<String, Any?>()
                if (json.has("parameters")) {
                    json["parameters"].asJsonObject.entrySet().forEach { (key, value) ->
                        parameters[key] = when {
                            value.isJsonPrimitive && value.asJsonPrimitive.isString -> value.asString
                            value.isJsonPrimitive && value.asJsonPrimitive.isBoolean -> value.asBoolean
                            value.isJsonPrimitive && value.asJsonPrimitive.isNumber -> value.asNumber
                            value.isJsonNull -> null
                            else -> value.toString()
                        }
                    }
                }
                if (json.has("offset_parameters")) {
                    json["offset_parameters"].asJsonObject.entrySet().forEach { (key, value) ->
                        offsetParameters[key] = when {
                            value.isJsonPrimitive && value.asJsonPrimitive.isString -> value.asString
                            value.isJsonPrimitive && value.asJsonPrimitive.isBoolean -> value.asBoolean
                            value.isJsonPrimitive && value.asJsonPrimitive.isNumber -> value.asNumber
                            value.isJsonNull -> null
                            else -> value.toString()
                        }
                    }
                }
                @Suppress("unchecked_cast") val function = if (json.has("function")) {
                    getFunctionByName(json["function"].asString)!!.invoke(
                        null,
                        parameters
                    ) as (Long) -> Sequence<Coordinates>
                } else {
                    { _ -> sequenceOf(Coordinates(0.0, 0.0, 0.0)) }
                }
                @Suppress("unchecked_cast") val offsetFunction = if (json.has("offset_function")) {
                    getFunctionByName(json["offset_function"].asString)!!.invoke(
                        null,
                        offsetParameters
                    ) as (Long) -> Sequence<Coordinates>
                } else {
                    { _ -> sequenceOf(Coordinates(0.0, 0.0, 0.0)) }
                }
                return Element(particleType, x, y, z, offsetX, offsetY, offsetZ, function, offsetFunction, count, delay, period, extra)
            }
        }
    }
}

data class Coordinates(val x: Double, val y: Double, val z: Double)

fun loadParticles(directory: File): Map<String, Particles> {
    val files = directory.listFiles() ?: emptyArray()
    return files.asSequence()
        .filter { it.name.endsWith(".json") }
        .associate {
            it.name.removeSuffix(".json") to gson.fromJson(it.reader(), Particles::class.java)
        }
}

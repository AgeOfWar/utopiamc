package mc.utopia.plugin.particles

import mc.utopia.plugin.util.runTaskTimer
import org.bukkit.GameMode
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.scheduler.BukkitTask

class ParticleStarter(private val plugin: ParticlesPlugin) : Listener {
    private val tasks = mutableMapOf<Player, BukkitTask>()

    fun addPlayer(player: Player) {
        var oldLocation = player.location
        var oldParticles: String? = null
        var moving = true
        tasks[player] = plugin.runTaskTimer(2L, 2L) {
            val oldX = oldLocation.x
            val oldY = oldLocation.y
            val oldZ = oldLocation.z
            val location = player.location
            val particles = plugin.getCurrentParticles(player)
            if (oldX == location.x && oldY == location.y && oldZ == location.z) {
                if (moving || oldParticles != particles) {
                    if (player.gameMode != GameMode.SPECTATOR) {
                        plugin.startParticles(player, plugin.particles[particles])
                    }
                }
                moving = false
            } else if (!moving) {
                plugin.startParticles(player, null)
                moving = true
            }
            oldLocation = player.location
            oldParticles = particles
        }
    }

    fun removePlayer(player: Player) {
        tasks[player]?.cancel()
        tasks.remove(player)
        plugin.startParticles(player, null)
    }

    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        addPlayer(event.player)
    }

    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        removePlayer(event.player)
    }
}
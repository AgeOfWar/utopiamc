package mc.utopia.plugin.particles

import java.lang.Math.toRadians
import java.lang.reflect.Method
import kotlin.math.cos
import kotlin.math.sin

fun getFunctionByName(name: String): Method? =
    Class.forName("mc.utopia.plugin.particles.FunctionsKt").methods.find { it.name == name }

fun circle(parameters: Map<String, Any?>) = { t: Long ->
    val r: Double by parameters
    val initialAlphaDegree: Double by parameters
    val deltaAlphaDegree: Double by parameters
    val initialAlpha = toRadians(initialAlphaDegree)
    val deltaAlpha = toRadians(deltaAlphaDegree)
    sequenceOf(
        Coordinates(
            x = r * cos(initialAlpha + t * deltaAlpha),
            y = 0.0,
            z = r * sin(initialAlpha + t * deltaAlpha)
        )
    )
}

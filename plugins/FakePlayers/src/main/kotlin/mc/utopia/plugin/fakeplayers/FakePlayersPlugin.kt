package mc.utopia.plugin.fakeplayers

import com.comphenix.protocol.PacketType
import com.comphenix.protocol.ProtocolLibrary
import com.comphenix.protocol.events.ListenerPriority
import com.comphenix.protocol.events.PacketAdapter
import com.comphenix.protocol.events.PacketEvent
import com.mojang.authlib.GameProfile
import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.registerEvents
import net.minecraft.server.v1_16_R3.*
import org.bukkit.Bukkit
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer
import org.bukkit.craftbukkit.v1_16_R3.scoreboard.CraftScoreboard
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.plugin.java.JavaPlugin
import java.util.*

val protocolLib = ProtocolLibrary.getProtocolManager()

class FakePlayersPlugin : JavaPlugin(), Listener {
    val config = fromJsonOrElse("config.json", Config())

    override fun onEnable() {
        registerEvents(this)
        protocolLib.addPacketListener(
            object : PacketAdapter(this, ListenerPriority.NORMAL, PacketType.Status.Server.SERVER_INFO) {
                override fun onPacketSending(event: PacketEvent) {
                    if (event.packetType != PacketType.Status.Server.SERVER_INFO) return
                    val serverPings = event.packet.serverPings.read(0)
                    serverPings.playersOnline += config.fakePlayers.size
                    event.packet.serverPings.write(0, serverPings)
                }
            }
        )
    }

    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val player = event.player
        player as CraftPlayer
        player.handle.playerConnection.sendPacket(PacketPlayOutPlayerInfo(
            PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, config.fakePlayers.map {
                EntityPlayer(
                    MinecraftServer.getServer(),
                    (Bukkit.getWorld("spawn") as CraftWorld).handle.minecraftWorld,
                    GameProfile(UUID.fromString(it.uuid), it.name),
                    PlayerInteractManager((Bukkit.getWorld("spawn") as CraftWorld).handle.minecraftWorld)
                )
            }
        ))
        config.fakePlayers.groupBy { it.team }.forEach { (team, fakePlayers) ->
            player.handle.playerConnection.sendPacket(PacketPlayOutScoreboardTeam(
                (Bukkit.getScoreboardManager()!!.mainScoreboard as CraftScoreboard).handle.getTeam(team),
                fakePlayers.map { it.name },
                3
            ))
        }
    }

    data class Config(
        val fakePlayers: List<FakePlayer> = emptyList()
    )
}

data class FakePlayer(val uuid: String, val name: String, val team: String)
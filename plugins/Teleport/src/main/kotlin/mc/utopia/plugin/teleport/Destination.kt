package mc.utopia.plugin.teleport

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import mc.utopia.plugin.util.gsonBuilder
import org.bukkit.Bukkit.getWorld
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.block.BlockFace
import org.bukkit.entity.Player
import org.bukkit.inventory.meta.CompassMeta
import java.io.File
import java.lang.Math.random
import java.lang.reflect.Type

val gson: Gson = gsonBuilder.registerTypeAdapter(Destination::class.java, Destination.Serializer).create()

interface Destination {
    fun toLocation(player: Player, vararg args: String): Location

    fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    object Serializer : JsonDeserializer<Destination>, JsonSerializer<Destination> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Destination {
            return when (val type = json.asJsonObject["type"].asString) {
                "location" -> context.deserialize(json, ExactLocation::class.java)
                "random" -> context.deserialize(json, RandomLocation::class.java)
                "bed" -> context.deserialize(json, BedLocation::class.java)
                "waypoint" -> context.deserialize(json, WaypointLocation::class.java)
                else -> throw JsonParseException("Unknown Destination '$type'")
            }
        }

        override fun serialize(destination: Destination, typeOfT: Type, context: JsonSerializationContext): JsonElement {
            return context.serialize(destination, destination.javaClass)
        }
    }
}

class InvalidBedException : Exception()
class InvalidCompassException : Exception()
class InvalidSpawnException : Exception()

data class ExactLocation(
    val world: String,
    val x: Double,
    val z: Double,
    val y: Double? = null,
    val yaw: Float = 0.0f,
    val pitch: Float = 0.0f
) : Destination {
    private val type = "location"

    override fun toLocation(player: Player, vararg args: String): Location {
        val world = getWorld(world)!!
        val x = x
        val z = z
        val y = this.y ?: world.getHighestBlockYAt(x.toInt(), z.toInt()).toDouble() + 1
        return Location(world, x, y, z, yaw, pitch)
    }
}

data class RandomLocation(
    val world: String,
    val x: Double,
    val z: Double,
    val y: Double? = null,
    val offsetX: Double,
    val offsetZ: Double,
    val yaw: Float = 0.0f,
    val pitch: Float = 0.0f
) : Destination {
    private val type = "random"

    override fun toLocation(player: Player, vararg args: String): Location {
        val world = getWorld(world)!!
        val x = x + offsetX * random()
        val z = z + offsetZ * random()
        val y = this.y ?: world.getHighestBlockYAt(x.toInt(), z.toInt()).toDouble() + 1
        val location = Location(world, x, y, z, yaw, pitch)
        return if (!location.canBeSpawn()) toLocation(player, *args) else location
    }
}

class BedLocation : Destination {
    private val type = "bed"

    override fun toLocation(player: Player, vararg args: String): Location {
        return player.bedSpawnLocation ?: throw InvalidBedException()
    }
}

class WaypointLocation : Destination {
    private val type = "waypoint"

    override fun toLocation(player: Player, vararg args: String): Location {
        val compass = player.inventory.itemInMainHand
        if (compass.type != Material.COMPASS) throw InvalidCompassException()
        val compassMeta = compass.itemMeta as? CompassMeta ?: throw InvalidCompassException()
        val lodestone = compassMeta.lodestone ?: throw InvalidCompassException()
        return lodestone.block.getLowestSpawnBlock().location.apply {
            x += 0.5
            z += 0.5
        }
    }
}

fun loadDestinations(file: File): MutableMap<String, Destination> {
    return if (!file.exists()) {
        mutableMapOf()
    } else file.reader().use {
        gson.fromJson<MutableMap<String, Destination>>(it, object : TypeToken<MutableMap<String, Destination>>() {}.type)
    }
}

fun Location.canBeSpawn(): Boolean {
    return Location(world, x, y + 1, z, yaw, pitch).block.type.isAir &&
            block.type.isAir &&
            Location(world, x, y - 1, z, yaw, pitch).block.type.isSolid
}

fun Block.getLowestSpawnBlock(): Block {
    val up = getRelative(BlockFace.UP)
    return when {
        y == world.maxHeight -> throw InvalidSpawnException()
        type.isSolid && up.type.isAir && up.getRelative(BlockFace.UP).type.isAir -> up
        else -> up.getLowestSpawnBlock()
    }
}

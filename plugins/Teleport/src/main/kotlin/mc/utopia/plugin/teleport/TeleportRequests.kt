package mc.utopia.plugin.teleport

import org.bukkit.entity.Player

class TeleportRequests(private val requests: MutableMap<Player, MutableSet<Player>> = mutableMapOf()) {
    fun addRequest(sender: Player, target: Player) {
        if (target !in requests) requests[target] = mutableSetOf()
        requests[target]!! += sender
    }

    fun removeRequest(sender: Player, target: Player) {
        if (target in requests) {
            requests[target]!! -= sender
            if (requests.isEmpty()) requests.remove(target)
        }
    }

    fun hasRequested(sender: Player, target: Player): Boolean {
        return sender in requests[target] ?: return false
    }

    operator fun get(player: Player): Set<Player> = requests[player] ?: emptySet()
}
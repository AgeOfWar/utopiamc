package mc.utopia.plugin.teleport

import mc.utopia.plugin.util.*
import org.bukkit.Bukkit.getPlayer
import org.bukkit.Sound
import org.bukkit.SoundCategory
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitTask
import java.util.*

fun teleportCommand(
    plugin: JavaPlugin,
    destinations: Map<String, Destination>,
    requestExpirationTime: Long,
    teleportDelay: Long,
    teleportWait: Long,
    requests: TeleportRequests,
    strings: TeleportCommandStrings
): TreeCommand {
    val tpToSenders = mutableSetOf<UUID>()
    val subCommands = mapOf(
        "to" to DelayedCommand(
            plugin, TeleportToCommand(plugin, destinations, teleportWait, strings.to, tpToSenders).also {
                plugin.registerEvents(it)
            }, teleportDelay, strings.wait, tpToSenders),
        "ask" to TeleportAskCommand(plugin, requestExpirationTime, requests, strings.ask),
        "accept" to TeleportAcceptCommand(requests, strings.accept)
    )
    return TreeCommand(subCommands, strings.syntax)
}

data class TeleportCommandStrings(
    val syntax: Text = emptyList(),
    val wait: Text = emptyList(),
    val ask: TeleportAskCommand.Strings = TeleportAskCommand.Strings(),
    val accept: TeleportAcceptCommand.Strings = TeleportAcceptCommand.Strings(),
    val to: TeleportToCommand.Strings = TeleportToCommand.Strings()
)

class TeleportAskCommand(
    private val plugin: JavaPlugin,
    private val requestExpirationTime: Long,
    private val requests: TeleportRequests,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.size != 1) {
            sender.sendMessage(strings.syntax)
            return false
        }
        val playerName = args[0]
        val target = getPlayer(playerName)
        if (target == null) {
            sender.sendMessage(strings.playerNotFound, mapOf("playerName" to playerName))
            return false
        }
        if (target == sender) {
            sender.sendMessage(strings.cannotAskSelf)
            return false
        }
        if (requests.hasRequested(sender, target)) {
            sender.sendMessage(strings.alreadyRequested, mapOf("playerName" to playerName))
            return false
        }
        requests.addRequest(sender, target)
        plugin.runTaskLater(requestExpirationTime) {
            if (requests.hasRequested(sender, target)) {
                requests.removeRequest(sender, target)
                sender.sendMessage(strings.requestExpired, mapOf("playerName" to playerName))
            }
        }
        sender.sendMessage(strings.message, mapOf("playerName" to playerName))
        target.sendMessage(strings.request, mapOf("senderName" to sender.name))
        target.playSound(target.location, Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, 1f, 1f)
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return if (args.size == 1) {
            (onlinePlayers - sender).map { it.name }
        } else {
            emptyList()
        }
    }

    data class Strings(
        val syntax: Text = emptyList(),
        val playerNotFound: Text = emptyList(),
        val cannotAskSelf: Text = emptyList(),
        val alreadyRequested: Text = emptyList(),
        val requestExpired: Text = emptyList(),
        val request: Text = emptyList(),
        val message: Text = emptyList()
    )
}

class TeleportAcceptCommand(
    private val requests: TeleportRequests,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.size != 1) {
            sender.sendMessage(strings.syntax)
            return false
        }
        val playerName = args[0]
        val target = getPlayer(playerName)
        if (target == null) {
            sender.sendMessage(strings.playerNotFound, mapOf("playerName" to playerName))
            return false
        }
        if (!requests.hasRequested(target, sender)) {
            sender.sendMessage(strings.notRequested, mapOf("playerName" to playerName))
            return false
        }
        if (!sender.isOnGround) {
            sender.sendMessage(strings.flying)
            return false
        }
        requests.removeRequest(target, sender)
        target.teleport(sender.location)
        target.sendMessage(strings.accepted, mapOf("senderName" to sender.name))
        sender.sendMessage(strings.message, mapOf("playerName" to playerName))
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return if (args.size == 1) {
            requests[sender].map { it.name }
        } else {
            emptyList()
        }
    }

    data class Strings(
        val syntax: Text = emptyList(),
        val playerNotFound: Text = emptyList(),
        val notRequested: Text = emptyList(),
        val accepted: Text = emptyList(),
        val flying: Text = emptyList(),
        val message: Text = emptyList()
    )
}

class TeleportToCommand(
    private val plugin: JavaPlugin,
    private val destinations: Map<String, Destination>,
    private val teleportWait: Long,
    private val strings: Strings,
    private val senders: MutableSet<UUID>
) : PlayerCommand, Listener {
    private val players = mutableMapOf<Player, BukkitTask>()

    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.size != 1) {
            sender.sendMessage(strings.syntax)
            return false
        }
        val destinationName = args[0]
        val destination = destinations[destinationName]
        if (destination == null) {
            sender.sendMessage(strings.unknownDestination, mapOf("destinationName" to destinationName))
            return false
        }
        plugin.runTaskAsynchronously {
            try {
                if (destination is RandomLocation) { // ugly solution for O(infinity) complexity
                    sender.sendMessage(strings.searchingLocation)
                }
                val location = destination.toLocation(sender)
                sender.sendMessage(strings.message)
                players[sender] = plugin.runTaskLater(teleportWait) {
                    players.remove(sender)
                    sender.teleport(location)
                }
            } catch (e: InvalidBedException) {
                plugin.runTask {
                    senders -= sender.uniqueId
                }
                sender.sendMessage(strings.missingOrObstructedBed)
            } catch (e: InvalidCompassException) {
                plugin.runTask {
                    senders -= sender.uniqueId
                }
                sender.sendMessage(strings.invalidCompass)
            } catch (e: InvalidSpawnException) {
                plugin.runTask {
                    senders -= sender.uniqueId
                }
                sender.sendMessage(strings.invalidSpawn)
            }
        }
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return if (args.size == 1) {
            destinations.keys.toList()
        } else {
            emptyList()
        }
    }

    @EventHandler
    fun onPlayerMove(event: PlayerMoveEvent) {
        val player = event.player
        val task = players[player]
        val from = event.from
        val to = event.to
        if (task != null && (from.blockX != to.blockX || from.blockY != to.blockY || from.blockZ != to.blockZ)) {
            task.cancel()
            player.sendMessage(strings.cancelled)
            players.remove(player)
            senders -= player.uniqueId
        }
    }

    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        val player = event.player
        if (players.remove(player) != null) {
            senders -= player.uniqueId
        }
    }

    data class Strings(
        val syntax: Text = emptyList(),
        val unknownDestination: Text = emptyList(),
        val missingOrObstructedBed: Text = emptyList(),
        val invalidCompass: Text = emptyList(),
        val invalidSpawn: Text = emptyList(),
        val searchingLocation: Text = emptyList(),
        val message: Text = emptyList(),
        val cancelled: Text = emptyList()
    )
}

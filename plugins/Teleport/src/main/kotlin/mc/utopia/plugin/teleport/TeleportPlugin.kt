package mc.utopia.plugin.teleport

import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.registerCommand
import org.bukkit.plugin.java.JavaPlugin

class TeleportPlugin : JavaPlugin() {
    override fun onEnable() {
        val config = fromJsonOrElse("config.json", Config())
        val teleportDestinations = loadDestinations(dataFolder.resolve("destinations.json"))
        val (tpaRequestExpirationTime, tpToTeleportDelay, tpToTeleportWait, tp) = config
        val requests = TeleportRequests()
        registerCommand("tp", teleportCommand(this, teleportDestinations, tpaRequestExpirationTime, tpToTeleportDelay, tpToTeleportWait, requests, tp))
    }

    data class Config(
        val tpAskRequestTimeout: Long = 1200L,
        val tpToTeleportDelay: Long = 1200L,
        val tpToTeleportWait: Long = 100L,
        val tp: TeleportCommandStrings = TeleportCommandStrings()
    )
}
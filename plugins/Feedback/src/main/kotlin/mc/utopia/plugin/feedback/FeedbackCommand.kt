package mc.utopia.plugin.feedback

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.methods.SendMessage
import io.github.ageofwar.telejam.text.Text.parseHtml
import mc.utopia.plugin.util.*
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin

class FeedbackCommand(
    private val plugin: JavaPlugin,
    private val bot: Bot,
    private val chat: Long,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.isEmpty()) {
            sender.sendMessage(strings.usage)
            return false
        }
        val text = args.joinToString(" ")
        val sendMessage = SendMessage()
            .chat(chat)
            .text(parseHtml(strings.format.format(mapOf("sender" to escapeHtml(sender.name), "message" to escapeHtml(text)))))
        plugin.runTaskAsynchronously {
            bot.execute(sendMessage)
        }
        sender.sendMessage(strings.thanks)
        return true
    }

    private fun escapeHtml(string: String) = io.github.ageofwar.telejam.text.Text(string).toHtmlString()

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    data class Strings(
        val usage: Text = emptyList(),
        val format: String = "\$sender: \$message",
        val thanks: Text = emptyList()
    )
}

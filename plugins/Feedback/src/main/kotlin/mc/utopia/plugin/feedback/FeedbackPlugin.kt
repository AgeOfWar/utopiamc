package mc.utopia.plugin.feedback

import io.github.ageofwar.telejam.Bot
import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.registerCommand
import org.bukkit.plugin.java.JavaPlugin

class FeedbackPlugin : JavaPlugin() {
    override fun onEnable() {
        val (botToken, chat, feedback) = fromJsonOrElse("config.json", Config())
        val bot = Bot.fromToken(botToken)
        registerCommand("feedback", FeedbackCommand(this, bot, chat, feedback))
    }

    data class Config(
        val botToken: String = "",
        val chat: Long = 123456789L,
        val feedback: FeedbackCommand.Strings = FeedbackCommand.Strings()
    )
}
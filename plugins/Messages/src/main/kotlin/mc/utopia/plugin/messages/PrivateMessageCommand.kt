package mc.utopia.plugin.messages

import mc.utopia.plugin.util.PlayerCommand
import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.onlinePlayers
import mc.utopia.plugin.util.sendMessage
import org.bukkit.Bukkit.getPlayer
import org.bukkit.entity.Player
import java.util.*

class PrivateMessageCommand(private val strings: Strings) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.size < 2) {
            sender.sendMessage(strings.usage)
            return false
        }
        val playerName = args[0]
        val player = getPlayer(playerName)
        if (player == null) {
            sender.sendMessage(strings.playerNotFound, mapOf("playerName" to playerName))
            return false
        }
        val message = StringJoiner(" ").apply {
            for (i in 1..args.lastIndex) {
                add(args[i])
            }
        }.toString()
        sender.sendMessage(strings.messageSent, mapOf("playerName" to player.name, "message" to message))
        player.sendMessage(strings.message, mapOf("senderName" to sender.name, "message" to message))
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return when (args.size) {
            1 -> onlinePlayers.map { it.name }
            else -> emptyList()
        }
    }

    data class Strings(
        val playerNotFound: Text = emptyList(),
        val messageSent: Text = emptyList(),
        val message: Text = emptyList(),
        val usage: Text = emptyList()
    )
}

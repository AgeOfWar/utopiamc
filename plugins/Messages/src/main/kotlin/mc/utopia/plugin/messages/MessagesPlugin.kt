package mc.utopia.plugin.messages

import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.registerCommand
import org.bukkit.plugin.java.JavaPlugin

class MessagesPlugin : JavaPlugin() {
    override fun onEnable() {
        val (msg) = fromJsonOrElse("config.json", Config())
        registerCommand("msg", PrivateMessageCommand(msg))
    }

    data class Config(val msg: PrivateMessageCommand.Strings = PrivateMessageCommand.Strings())
}
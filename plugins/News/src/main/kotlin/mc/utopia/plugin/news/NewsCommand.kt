package mc.utopia.plugin.news

import mc.utopia.plugin.util.PlayerCommand
import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.sendMessage
import org.bukkit.entity.Player

class NewsCommand(
    private val plugin: NewsPlugin,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        val news = plugin.getNews()
        if (news.isEmpty()) {
            sender.sendMessage(strings.empty)
        } else {
            sender.sendMessage(news)
        }
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return emptyList()
    }

    data class Strings(val empty: Text = listOf())
}

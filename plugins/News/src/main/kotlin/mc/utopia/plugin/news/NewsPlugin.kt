package mc.utopia.plugin.news

import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.registerCommand
import mc.utopia.plugin.util.registerEvents
import org.bukkit.plugin.java.JavaPlugin

class NewsPlugin : JavaPlugin() {
    override fun onEnable() {
        val (news) = fromJsonOrElse("config.json", Config())
        registerEvents(NewsDisplay(this))
        registerCommand("news", NewsCommand(this, news))
    }

    fun getNews(): Text {
        return fromJsonOrElse<Text>("news.json", emptyList())
    }

    data class Config(
        val news: NewsCommand.Strings = NewsCommand.Strings()
    )
}
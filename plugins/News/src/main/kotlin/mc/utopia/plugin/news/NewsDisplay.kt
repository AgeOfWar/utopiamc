package mc.utopia.plugin.news

import mc.utopia.plugin.util.runTask
import mc.utopia.plugin.util.sendMessage
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class NewsDisplay(private val plugin: NewsPlugin) : Listener {
    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val player = event.player
        if (!player.hasPlayedBefore()) return
        val news = plugin.getNews()
        if (news.isNotEmpty()) {
            plugin.runTask {
                player.sendMessage(news)
            }
        }
    }
}
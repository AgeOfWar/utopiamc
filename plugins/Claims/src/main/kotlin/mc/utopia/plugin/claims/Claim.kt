package mc.utopia.plugin.claims

import org.bukkit.Bukkit.getOfflinePlayer
import org.bukkit.Chunk
import java.util.*

data class Claim(
    val owner: UUID,
    val world: String,
    val x: Int,
    val z: Int
)

data class ClaimChunk(
    val world: String,
    val x: Int,
    val z: Int
)

val Chunk.claimChunk get() = ClaimChunk(world.name, x, z)

val Claim.ownerName get() = getOfflinePlayer(owner).name!!
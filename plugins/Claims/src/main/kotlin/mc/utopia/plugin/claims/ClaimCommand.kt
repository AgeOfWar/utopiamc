package mc.utopia.plugin.claims

import mc.utopia.plugin.util.*
import net.minecraft.server.v1_16_R3.PacketPlayOutWorldBorder
import net.minecraft.server.v1_16_R3.WorldBorder
import org.bukkit.Bukkit.getPlayer
import org.bukkit.command.CommandSender
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer
import org.bukkit.entity.Player
import org.bukkit.event.Listener
import org.bukkit.plugin.java.JavaPlugin

fun claimCommand(
    plugin: JavaPlugin,
    data: ClaimsData,
    claimableWorlds: List<String>,
    strings: ClaimCommandStrings
): TreeCommand {
    val subCommands = mapOf(
        "add" to ClaimAddCommand(plugin, data, claimableWorlds, strings.add).also { plugin.registerEvents(it) },
        "remove" to ClaimRemoveCommand(data, strings.remove),
        "view" to ClaimViewCommand(plugin, data, strings.view),
        "allow" to ClaimAllowCommand(data, strings.allow, true),
        "deny" to ClaimAllowCommand(data, strings.allow, false),
        "permissions" to ClaimPermissionsCommand(data, strings.permissions),
        "info" to ClaimInfoCommand(data, strings.info),
        "set" to AdminCommand(ClaimSetCommand(data, strings.set), strings.usage)
    )
    return TreeCommand(subCommands, strings.usage)
}

data class ClaimCommandStrings(
    val add: ClaimAddCommand.Strings = ClaimAddCommand.Strings(),
    val remove: ClaimRemoveCommand.Strings = ClaimRemoveCommand.Strings(),
    val view: ClaimViewCommand.Strings = ClaimViewCommand.Strings(),
    val usage: Text = emptyList(),
    val allow: ClaimAllowCommand.Strings = ClaimAllowCommand.Strings(),
    val permissions: ClaimPermissionsCommand.Strings = ClaimPermissionsCommand.Strings(),
    val info: ClaimInfoCommand.Strings = ClaimInfoCommand.Strings(),
    val set: ClaimSetCommand.Strings = ClaimSetCommand.Strings()
)

class ClaimAddCommand(
    private val plugin: JavaPlugin,
    private val data: ClaimsData,
    private val claimableWorlds: List<String>,
    private val strings: Strings
) : PlayerCommand, Listener {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (sender.world.name !in claimableWorlds) {
            sender.sendMessage(strings.notClaimableWorld)
            return false
        }
        val chunk = sender.location.chunk.claimChunk
        data.addClaim(sender.uniqueId, chunk) {
            when (it) {
                is AlreadyClaimed -> {
                    sender.sendMessage(strings.alreadyClaimed, mapOf("owner" to it.owner))
                }
                is NotEnoughClaims -> {
                    sender.sendMessage(strings.notEnoughBlocks)
                }
                is Success -> {
                    sender.showArea(plugin, chunk, 20L * strings.claimViewTime)
                    sender.sendMessage(strings.message, mapOf("remained" to it.remainedClaims.toString()))
                }
            }
        }
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    data class Strings(
        val notClaimableWorld: Text = emptyList(),
        val notEnoughBlocks: Text = emptyList(),
        val alreadyClaimed: Text = emptyList(),
        val claimViewTime: Long = 3,
        val message: Text = emptyList()
    )
}

class ClaimRemoveCommand(
    private val data: ClaimsData,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        when (args.size) {
            0 -> {
                val claim = data.getClaim(sender.location.chunk.claimChunk)
                if (claim?.owner != sender.uniqueId) {
                    sender.sendMessage(strings.notInClaim)
                    return false
                }
                data.removeClaim(claim)
                sender.sendMessage(strings.message)
                return true
            }
            1 -> return if (args[0] == "all") {
                sender.sendMessage(strings.removeAllConfirm)
                true
            } else {
                sender.sendMessage(strings.usage)
                false
            }
            2 -> return if (args[0] == "all" && args[1] == "confirm") {
                data.removeAllClaims(sender.uniqueId)
                sender.sendMessage(strings.removeAllMessage)
                true
            } else {
                sender.sendMessage(strings.usage)
                false
            }
            else -> {
                sender.sendMessage(strings.usage)
                return false
            }
        }
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return if (args.size == 1) listOf("all") else emptyList()
    }

    data class Strings(
        val usage: Text = emptyList(),
        val notInClaim: Text = emptyList(),
        val message: Text = emptyList(),
        val removeAllConfirm: Text = emptyList(),
        val removeAllMessage: Text = emptyList()
    )
}

class ClaimViewCommand(
    private val plugin: JavaPlugin,
    private val data: ClaimsData,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        val claim = data.getClaim(sender.location.chunk.claimChunk)
        if (claim == null) {
            sender.sendMessage(strings.notInClaim)
            return false
        }
        val time = when (args.size) {
            0 -> strings.claimViewTime
            1 -> args[0].toLongOrNull()?.coerceIn(0L..120L)
            else -> null
        }
        if (time == null) {
            sender.sendMessage(strings.usage)
            return false
        }
        sender.showArea(plugin, sender.location.chunk.claimChunk, 20L * time)
        sender.sendMessage(strings.message, mapOf("owner" to claim.ownerName))
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    data class Strings(
        val usage: Text = emptyList(),
        val message: Text = emptyList(),
        val notInClaim: Text = emptyList(),
        val claimViewTime: Long = 3
    )
}

class ClaimInfoCommand(
    private val data: ClaimsData,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        sender.sendMessage(strings.message, mapOf(
            "max" to data.getMaxClaims(sender.uniqueId).toString(),
            "remained" to data.getRemainedClaims(sender.uniqueId).toString(),
            "used" to data.getUsedClaims(sender.uniqueId).toString()
        ))
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    data class Strings(
        val message: Text = emptyList()
    )
}

class ClaimAllowCommand(
    private val data: ClaimsData,
    private val strings: Strings,
    private val allow: Boolean
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.size < 2) {
            sender.sendMessage(strings.usage)
            return false
        }
        val player = getPlayer(args[0])
        if (player == null) {
            sender.sendMessage(strings.invalidPlayer, mapOf("playerName" to args[0]))
            return false
        }
        val permissions = data.getPermissions(sender.uniqueId, player.uniqueId)
        when (args[1]) {
            "edit" -> data.setPermissions(sender.uniqueId, player.uniqueId, permissions.withCanEdit(allow))
            "interact" -> data.setPermissions(sender.uniqueId, player.uniqueId, permissions.withCanInteract(allow))
            "openChests" -> data.setPermissions(sender.uniqueId, player.uniqueId, permissions.withCanOpenChests(allow))
            "damageMobs" -> data.setPermissions(sender.uniqueId, player.uniqueId, permissions.withCanDamageMobs(allow))
            else -> {
                sender.sendMessage(strings.unknownPermission)
                return false
            }
        }
        sender.sendMessage(strings.message)
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return when (args.size) {
            1 -> onlinePlayers.map { it.name }
            2 -> listOf("edit", "interact", "openChests", "damageMobs")
            else -> emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val invalidPlayer: Text = emptyList(),
        val unknownPermission: Text = emptyList(),
        val message: Text = emptyList()
    )
}

class ClaimPermissionsCommand(
    private val data: ClaimsData,
    private val strings: Strings
): PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.size != 1) {
            sender.sendMessage(strings.usage)
            return false
        }
        val player = getPlayer(args[0])
        if (player == null) {
            sender.sendMessage(strings.invalidPlayer, mapOf("playerName" to args[0]))
            return false
        }
        val permissions = data.getPermissions(sender.uniqueId, player.uniqueId)
        sender.sendMessage(strings.message, mapOf(
            "playerName" to player.name,
            "canEdit" to permissions.canEdit.toString(),
            "canInteract" to permissions.canInteract.toString(),
            "canOpenChests" to permissions.canOpenChests.toString(),
            "canDamageMobs" to permissions.canDamageMobs.toString()
        ))
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return if (args.size == 1) {
            onlinePlayers.map { it.name }
        } else {
            emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val invalidPlayer: Text = emptyList(),
        val message: Text = emptyList()
    )
}

class ClaimSetCommand(
        private val data: ClaimsData,
        private val strings: Strings
) : ConsoleCommand {
    override fun onCommand(sender: CommandSender, name: String, vararg args: String): Boolean {
        if (args.size != 2) {
            sender.sendMessage(strings.usage)
            return false
        }
        val player = getPlayer(args[0])
        if (player == null) {
            sender.sendMessage(strings.invalidPlayer, mapOf("playerName" to args[0]))
            return false
        }
        val maxClaims = args[1].toIntOrNull()
        if (maxClaims == null) {
            sender.sendMessage(strings.invalidValue)
            return false
        }
        data.setMaxClaims(player.uniqueId, maxClaims)
        sender.sendMessage(strings.message)
        return true
    }

    override fun onTabComplete(sender: CommandSender, name: String, vararg args: String) = when (args.size) {
        1 -> onlinePlayers.map { it.name }
        else -> emptyList()
    }

    data class Strings(
            val message: Text = emptyList(),
            val usage: Text = emptyList(),
            val invalidPlayer: Text = emptyList(),
            val invalidValue: Text = emptyList()
    )
}

fun Player.showArea(plugin: JavaPlugin, chunk: ClaimChunk, time: Long) {
    fun Player.sendWorldBorder(worldBorder: WorldBorder) {
        this as CraftPlayer
        val packet = PacketPlayOutWorldBorder(worldBorder, PacketPlayOutWorldBorder.EnumWorldBorderAction.INITIALIZE)
        handle.playerConnection.sendPacket(packet)
    }
    if (time > 0) {
        val worldBorder = WorldBorder()
        worldBorder.world = (world as CraftWorld).handle
        worldBorder.setCenter(chunk.x * 16.0 + 8.0, chunk.z * 16.0 + 8.0)
        worldBorder.size = 16.0
        worldBorder.damageAmount = 0.0
        worldBorder.damageBuffer = 0.0
        worldBorder.warningDistance = 0
        worldBorder.warningTime = 0
        sendWorldBorder(worldBorder)
    }
    plugin.runTaskLaterAsynchronously(time) {
        val resetWorldBorder = WorldBorder()
        resetWorldBorder.world = (world as CraftWorld).handle
        sendWorldBorder(resetWorldBorder)
    }
}

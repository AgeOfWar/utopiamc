package mc.utopia.plugin.claims

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent

class InitialPlayerClaims(
    private val data: ClaimsData,
    private val initialPlayerClaims: Int
) : Listener {
    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val player = event.player
        if (data.getMaxClaims(player.uniqueId) < initialPlayerClaims) {
            data.setMaxClaims(player.uniqueId, initialPlayerClaims)
        }
    }
}
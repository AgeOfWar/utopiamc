package mc.utopia.plugin.claims

import com.destroystokyo.paper.event.entity.EntityAddToWorldEvent
import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.sendMessage
import org.bukkit.Material
import org.bukkit.block.Container
import org.bukkit.block.data.type.Dispenser
import org.bukkit.entity.*
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.block.*
import org.bukkit.event.entity.CreatureSpawnEvent
import org.bukkit.event.entity.EntityChangeBlockEvent
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.EntityExplodeEvent
import org.bukkit.event.hanging.HangingBreakByEntityEvent
import org.bukkit.event.hanging.HangingPlaceEvent
import org.bukkit.event.player.PlayerInteractEntityEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.event.weather.LightningStrikeEvent
import org.bukkit.projectiles.BlockProjectileSource
import java.util.*

class ClaimProtection(private val data: ClaimsData, private val strings: Strings) : Listener {
    private val explosions = WeakHashMap<Entity, Claim?>()

    @EventHandler
    fun onPlayerInteract(event: PlayerInteractEvent) {
        val player = event.player
        val action = event.action
        val block = event.clickedBlock
        val claim = data.getClaim(block?.chunk?.claimChunk ?: player.location.chunk.claimChunk) ?: return
        val permissions = data.getPermissions(claim.owner, player.uniqueId)
        when {
            action == Action.RIGHT_CLICK_BLOCK && block?.state is Container && !permissions.canOpenChests -> {
                player.sendMessage(strings.cannotOpenChest, mapOf("owner" to claim.ownerName))
                event.isCancelled = true
            }
            action == Action.RIGHT_CLICK_BLOCK && block!!.blockData.material.isInteractable && !permissions.canInteract -> {
                player.sendMessage(strings.cannotInteract, mapOf("owner" to claim.ownerName))
                event.isCancelled = true
            }
            action == Action.PHYSICAL && !permissions.canInteract -> {
                event.isCancelled = true
            }
        }
    }

    @EventHandler
    fun onBlockPlace(event: BlockPlaceEvent) {
        val player = event.player
        val block = event.block
        val claim = data.getClaim(block.chunk.claimChunk) ?: return
        val permissions = data.getPermissions(claim.owner, player.uniqueId)
        if (!permissions.canEdit) {
            player.sendMessage(strings.cannotEdit, mapOf("owner" to claim.ownerName))
            event.isCancelled = true
        }
    }

    @EventHandler
    fun onBlockBreak(event: BlockDamageEvent) {
        val player = event.player
        val block = event.block
        val claim = data.getClaim(block.chunk.claimChunk) ?: return
        val permissions = data.getPermissions(claim.owner, player.uniqueId)
        if (!permissions.canEdit) {
            player.sendMessage(strings.cannotEdit, mapOf("owner" to claim.ownerName))
            event.isCancelled = true
        }
    }

    @EventHandler
    fun onBlockIgnite(event: BlockIgniteEvent) {
        val ignitingBlock = event.ignitingBlock
        val ignitingEntity = event.ignitingEntity
        val block = event.block
        val claim = data.getClaim(block.chunk.claimChunk) ?: return
        when {
            ignitingBlock != null -> {
                if (data.getClaim(ignitingBlock.chunk.claimChunk)?.owner != claim.owner) {
                    event.isCancelled = true
                }
            }
            ignitingEntity != null -> {
                when (ignitingEntity) {
                    is Player -> {
                        val permissions = data.getPermissions(claim.owner, ignitingEntity.uniqueId)
                        if (!permissions.canEdit) {
                            ignitingEntity.sendMessage(strings.cannotEdit, mapOf("owner" to claim.ownerName))
                            event.isCancelled = true
                        }
                    }
                    is Mob -> {
                        val target = ignitingEntity.target as? Player ?: return
                        val permissions = data.getPermissions(claim.owner, target.uniqueId)
                        if (!permissions.canEdit) {
                            event.isCancelled = true
                        }
                    }
                    is Projectile -> {
                        when (val shooter = ignitingEntity.shooter) {
                            is Player -> {
                                val permissions = data.getPermissions(claim.owner, shooter.uniqueId)
                                if (!permissions.canEdit) {
                                    ignitingEntity.sendMessage(strings.cannotEdit, mapOf("owner" to claim.ownerName))
                                    event.isCancelled = true
                                }
                            }
                            is Mob -> {
                                val target = shooter.target as? Player ?: return
                                val permissions = data.getPermissions(claim.owner, target.uniqueId)
                                if (!permissions.canEdit) {
                                    event.isCancelled = true
                                }
                            }
                            is BlockProjectileSource -> {
                                val oldBlock = shooter.block
                                val oldClaim = data.getClaim(oldBlock.chunk.claimChunk)
                                if (claim.owner != oldClaim?.owner) {
                                    event.isCancelled = true
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    fun onEntityExplode(event: EntityExplodeEvent) {
        val entity = event.entity
        if (entity !in explosions) return
        val oldClaim = explosions.remove(entity)
        event.blockList().removeIf {
            val claim = data.getClaim(it.chunk.claimChunk) ?: return@removeIf false
            claim.owner != oldClaim?.owner
        }
    }

    @EventHandler
    fun onTntActivated(event: EntityAddToWorldEvent) {
        val entity = event.entity
        if (entity.type != EntityType.PRIMED_TNT && entity.type != EntityType.MINECART_TNT) return
        explosions[entity] = data.getClaim(entity.location.chunk.claimChunk)
    }

    @EventHandler
    fun onWitherSpawn(event: CreatureSpawnEvent) {
        val entity = event.entity as? Wither ?: return
        explosions[entity] = data.getClaim(entity.location.chunk.claimChunk)
    }

    @EventHandler
    fun onWitherChange(event: EntityChangeBlockEvent) {
        val entity = event.entity
        if (entity.type != EntityType.WITHER && entity.type != EntityType.WITHER_SKULL) return
        val block = event.block
        val claim = data.getClaim(block.chunk.claimChunk)
        if (claim != null) {
            event.isCancelled = true
        }
    }

    @EventHandler
    fun onMobExplode(event: EntityExplodeEvent) {
        val entity = event.entity
        if (entity !is Mob) return
        val target = entity.target as? Player ?: return
        event.blockList().removeIf {
            val claim = data.getClaim(it.chunk.claimChunk) ?: return@removeIf false
            val permissions = data.getPermissions(claim.owner, target.uniqueId)
            !permissions.canEdit
        }
    }

    @EventHandler
    fun onProjectileExplode(event: EntityExplodeEvent) {
        val entity = event.entity as? Projectile ?: return
        when (val shooter = entity.shooter) {
            is Player -> {
                event.blockList().removeIf {
                    val claim = data.getClaim(it.chunk.claimChunk) ?: return@removeIf false
                    val permissions = data.getPermissions(claim.owner, shooter.uniqueId)
                    !permissions.canEdit
                }
            }
            is Wither -> {
                event.blockList().removeIf {
                    data.getClaim(it.chunk.claimChunk) != null
                }
            }
            is Mob -> {
                val target = shooter.target as? Player ?: return
                event.blockList().removeIf {
                    val claim = data.getClaim(it.chunk.claimChunk) ?: return@removeIf false
                    val permissions = data.getPermissions(claim.owner, target.uniqueId)
                    !permissions.canEdit
                }
            }
            is BlockProjectileSource -> {
                val block = shooter.block
                val oldClaim = data.getClaim(block.chunk.claimChunk)
                event.blockList().removeIf {
                    val claim = data.getClaim(it.chunk.claimChunk) ?: return@removeIf false
                    claim.owner != oldClaim?.owner
                }
            }
        }
    }

    @EventHandler
    fun onArrowActivateTnt(event: EntityChangeBlockEvent) {
        val entity = event.entity
        val block = event.block
        if (entity !is Projectile || block.type != Material.TNT) return
        val claim = data.getClaim(block.chunk.claimChunk) ?: return
        when (val shooter = entity.shooter) {
            is Player -> {
                val permissions = data.getPermissions(claim.owner, shooter.uniqueId)
                if (!permissions.canEdit) event.isCancelled = true
            }
            is Mob -> {
                val target = shooter.target as? Player ?: return
                val permissions = data.getPermissions(claim.owner, target.uniqueId)
                if (!permissions.canEdit) event.isCancelled = true
            }
            is BlockProjectileSource -> {
                val shooterBlock = shooter.block
                val oldClaim = data.getClaim(shooterBlock.chunk.claimChunk)
                if (claim.owner != oldClaim?.owner) event.isCancelled = true
            }
        }
    }

    @EventHandler
    fun onPistonExtend(event: BlockPistonExtendEvent) {
        val block = event.block
        val blocks = event.blocks
        val claim = data.getClaim(block.chunk.claimChunk)
        val cancel = blocks.any {
            val oldBlockClaim = data.getClaim(it.chunk.claimChunk)
            val blockClaim = data.getClaim(it.getRelative(event.direction).chunk.claimChunk)
            blockClaim != null && blockClaim.owner != claim?.owner ||
                    oldBlockClaim != null && oldBlockClaim.owner != claim?.owner
        }
        if (cancel) event.isCancelled = true
    }

    @EventHandler
    fun onPistonRetract(event: BlockPistonRetractEvent) {
        val block = event.block
        val blocks = event.blocks
        val claim = data.getClaim(block.chunk.claimChunk)
        val cancel = blocks.any {
            val blockClaim = data.getClaim(it.chunk.claimChunk)
            blockClaim != null && blockClaim.owner != claim?.owner
        }
        if (cancel) event.isCancelled = true
    }

    @EventHandler
    fun onBlockFromTo(event: BlockFromToEvent) {
        val from = event.block
        val to = event.toBlock
        val claim = data.getClaim(to.chunk.claimChunk) ?: return
        if (data.getClaim(from.chunk.claimChunk)?.owner != claim.owner) {
            event.isCancelled = true
        }
    }

    @EventHandler
    fun onBlockDispense(event: BlockDispenseEvent) {
        val block = event.block
        val face = (block.blockData as Dispenser).facing
        val item = event.item.type
        if (item != Material.TNT) return
        val claim = data.getClaim(block.getRelative(face).chunk.claimChunk) ?: return
        val oldClaim = data.getClaim(block.chunk.claimChunk)
        if (claim.owner != oldClaim?.owner) {
            event.isCancelled = true
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    fun onEntityDamageEntity(event: EntityDamageByEntityEvent) {
        val damager = event.damager
        val entity = event.entity
        if (entity is Player) return
        val claim = data.getClaim(entity.location.chunk.claimChunk) ?: return
        when (entity) {
            is Mob -> {
                val player = when (damager) {
                    is Player -> damager
                    is Projectile -> when (damager.shooter) {
                        is Player -> damager.shooter as Player
                        is Wither -> {
                            event.isCancelled = true
                            return
                        }
                        else -> return
                    }
                    is Explosive -> {
                        val oldClaim = explosions[damager]
                        if (oldClaim?.owner != claim.owner) {
                            event.isCancelled = true
                        }
                        return
                    }
                    is Wither -> {
                        event.isCancelled = true
                        return
                    }
                    else -> return
                }
                val permissions = data.getPermissions(claim.owner, player.uniqueId)
                if (!permissions.canDamageMobs) {
                    player.sendMessage(strings.cannotDamageEntity, mapOf("owner" to claim.ownerName))
                    event.isCancelled = true
                }
            }
            else -> {
                val player = when (damager) {
                    is Player -> damager
                    is Projectile -> when (damager.shooter) {
                        is Player -> damager.shooter as Player
                        is Wither -> {
                            event.isCancelled = true
                            return
                        }
                        else -> return
                    }
                    is Explosive -> {
                        val oldClaim = explosions[damager]
                        if (oldClaim?.owner != claim.owner) {
                            event.isCancelled = true
                        }
                        return
                    }
                    else -> return
                }
                val permissions = data.getPermissions(claim.owner, player.uniqueId)
                if (!permissions.canEdit) {
                    player.sendMessage(strings.cannotEdit, mapOf("owner" to claim.ownerName))
                    event.isCancelled = true
                }
            }
        }
    }

    @EventHandler
    fun onPlayerInteractEntity(event: PlayerInteractEntityEvent) {
        val entity = event.rightClicked
        val player = event.player
        val claim = data.getClaim(entity.location.chunk.claimChunk) ?: return
        val permissions = data.getPermissions(claim.owner, player.uniqueId)
        if (!permissions.canInteract) {
            player.sendMessage(strings.cannotInteract, mapOf("owner" to claim.ownerName))
            event.isCancelled = true
        }
    }

    @EventHandler
    fun onHangingPlace(event: HangingPlaceEvent) {
        val entity = event.entity
        val player = event.player ?: return
        val claim = data.getClaim(entity.location.chunk.claimChunk) ?: return
        val permissions = data.getPermissions(claim.owner, player.uniqueId)
        if (!permissions.canEdit) {
            player.sendMessage(strings.cannotEdit, mapOf("owner" to claim.ownerName))
            event.isCancelled = true
        }
    }

    @EventHandler
    fun onHangingBreak(event: HangingBreakByEntityEvent) {
        val entity = event.entity
        val remover = event.remover
        val claim = data.getClaim(entity.location.chunk.claimChunk) ?: return
        when (remover) {
            is Player -> {
                val permissions = data.getPermissions(claim.owner, remover.uniqueId)
                if (!permissions.canEdit) {
                    remover.sendMessage(strings.cannotEdit, mapOf("owner" to claim.ownerName))
                    event.isCancelled = true
                }
            }
            is Mob -> {
                val target = remover.target as? Player ?: return
                val permissions = data.getPermissions(claim.owner, target.uniqueId)
                if (!permissions.canEdit) {
                    event.isCancelled = true
                }
            }
            is Projectile -> {
                when (val shooter = remover.shooter) {
                    is Player -> {
                        val permissions = data.getPermissions(claim.owner, shooter.uniqueId)
                        if (!permissions.canEdit) {
                            remover.sendMessage(strings.cannotEdit, mapOf("owner" to claim.ownerName))
                            event.isCancelled = true
                        }
                    }
                    is Mob -> {
                        val target = shooter.target as? Player ?: return
                        val permissions = data.getPermissions(claim.owner, target.uniqueId)
                        if (!permissions.canEdit) {
                            event.isCancelled = true
                        }
                    }
                    is BlockProjectileSource -> {
                        val oldBlock = shooter.block
                        val oldClaim = data.getClaim(oldBlock.location.chunk.claimChunk)
                        if (claim.owner != oldClaim?.owner) {
                            event.isCancelled = true
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    fun onLightningStrike(event: LightningStrikeEvent) {
        if (event.cause != LightningStrikeEvent.Cause.TRIDENT) return
        val claim = data.getClaim(event.lightning.location.chunk.claimChunk) ?: return
        event.isCancelled = true
    }

    data class Strings(
        val cannotEdit: Text = emptyList(),
        val cannotInteract: Text = emptyList(),
        val cannotOpenChest: Text = emptyList(),
        val cannotDamageEntity: Text = emptyList()
    )
}

package mc.utopia.plugin.claims

import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.format
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerMoveEvent

class ClaimNotification(private val data: ClaimsData, private val strings: Strings) : Listener {
    @EventHandler
    fun onPlayerMove(event: PlayerMoveEvent) {
        val from = data.getClaim(event.from.chunk.claimChunk)
        val to = data.getClaim(event.to.chunk.claimChunk)
        if (from?.owner != to?.owner) {
            val player = event.player
            if (to == null) {
                player.sendActionBar(*strings.exitingClaim.format(mapOf("ownerName" to from!!.ownerName)).toTypedArray())
            } else {
                player.sendActionBar(*strings.enteringClaim.format(mapOf("ownerName" to to.ownerName)).toTypedArray())
            }
        }
    }

    data class Strings(
        val enteringClaim: Text = emptyList(),
        val exitingClaim: Text = emptyList()
    )
}
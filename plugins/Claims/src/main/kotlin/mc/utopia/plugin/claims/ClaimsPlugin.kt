package mc.utopia.plugin.claims

import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.registerCommand
import mc.utopia.plugin.util.registerEvents
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin

class ClaimsPlugin : JavaPlugin() {
    private val data = ClaimsData(this, dataFolder.resolve("claims.db"))

    fun getClaimsMaxSize(player: Player) = data.getMaxClaims(player.uniqueId)
    fun setClaimsMaxSize(player: Player, size: Int) = data.setMaxClaims(player.uniqueId, size)

    override fun onEnable() {
        val (claim, claimProtection, claimNotification, initialPlayerClaims, claimableWorlds) = fromJsonOrElse("config.json", Config())
        registerEvents(InitialPlayerClaims(data, initialPlayerClaims))
        registerEvents(ClaimProtection(data, claimProtection))
        registerEvents(ClaimNotification(data, claimNotification))
        registerCommand("claim", claimCommand(this, data, claimableWorlds, claim))
    }

    data class Config(
        val claim: ClaimCommandStrings = ClaimCommandStrings(),
        val claimProtection: ClaimProtection.Strings = ClaimProtection.Strings(),
        val claimNotification: ClaimNotification.Strings = ClaimNotification.Strings(),
        val initialPlayerClaims: Int = 0,
        val claimableWorlds: List<String> = emptyList()
    )
}

package mc.utopia.plugin.claims

import mc.utopia.plugin.util.runTaskAsynchronously
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.*

class ClaimsData(private val plugin: JavaPlugin, file: File) {
    private val url = "jdbc:sqlite:${file.canonicalPath}"

    private val permissions = mutableMapOf<Pair<UUID, UUID>, ClaimPermissions>()
    private val claims = mutableMapOf<ClaimChunk, Claim>()
    private val maxClaims = mutableMapOf<UUID, Int>()

    init {
        file.parentFile.mkdirs()
        executeUpdate("""
            CREATE TABLE IF NOT EXISTS claim (
                player_uuid_most bigint NOT NULL,
                player_uuid_least bigint NOT NULL,
                world varchar(45) NOT NULL,
            	x int NOT NULL,
                z int NOT NULL,
                PRIMARY KEY (x, z)
            )
        """.trimIndent())
        executeUpdate("""
            CREATE TABLE IF NOT EXISTS player_max_claims (
                player_uuid_most bigint NOT NULL,
                player_uuid_least bigint NOT NULL,
            	max_claims int NOT NULL,
                PRIMARY KEY (player_uuid_most, player_uuid_least)
            )
        """.trimIndent())
        executeUpdate("""
            CREATE TABLE IF NOT EXISTS permissions (
                owner_uuid_most bigint NOT NULL,
                owner_uuid_least bigint NOT NULL,
                player_uuid_most bigint NOT NULL,
                player_uuid_least bigint NOT NULL,
            	can_edit boolean NOT NULL,
                can_interact boolean NOT NULL,
                can_open_chests boolean NOT NULL,
                can_damage_mobs boolean NOT NULL,
                PRIMARY KEY (owner_uuid_most, owner_uuid_least, player_uuid_most, player_uuid_least)
            )
        """.trimIndent())
        executeQuery("SELECT * FROM permissions") {
            while (next()) {
                val owner = UUID(getLong("owner_uuid_most"), getLong("owner_uuid_least"))
                val player = UUID(getLong("player_uuid_most"), getLong("player_uuid_least"))
                val canEdit = getBoolean("can_edit")
                val canInteract = getBoolean("can_interact")
                val canOpenChests = getBoolean("can_open_chests")
                val canDamageMobs = getBoolean("can_damage_mobs")
                permissions[owner to player] = ClaimPermissions(canEdit, canInteract, canOpenChests, canDamageMobs)
            }
        }
        executeQuery("SELECT * FROM claim") {
            while (next()) {
                val player = UUID(getLong("player_uuid_most"), getLong("player_uuid_least"))
                val world = getString("world")
                val x = getInt("x")
                val z = getInt("z")
                claims[ClaimChunk(world, x, z)] = Claim(player, world, x, z)
            }
        }
        executeQuery("SELECT * FROM player_max_claims") {
            while (next()) {
                val player = UUID(getLong("player_uuid_most"), getLong("player_uuid_least"))
                val playerMaxClaims = getInt("max_claims")
                maxClaims[player] = playerMaxClaims
            }
        }
    }

    fun getPermissions(owner: UUID, player: UUID) = permissions[owner to player] ?: if (player == owner) {
        ClaimPermissions.all()
    } else ClaimPermissions()

    fun setPermissions(owner: UUID, player: UUID, permissions: ClaimPermissions) {
        this.permissions[owner to player] = permissions
        plugin.runTaskAsynchronously {
            executeUpdate("""
                    INSERT OR REPLACE INTO permissions(owner_uuid_most, owner_uuid_least, player_uuid_most, player_uuid_least, can_edit, can_interact, can_open_chests, can_damage_mobs)
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?)
                """.trimIndent()) {
                setLong(1, owner.mostSignificantBits)
                setLong(2, owner.leastSignificantBits)
                setLong(3, player.mostSignificantBits)
                setLong(4, player.leastSignificantBits)
                setBoolean(5, permissions.canEdit)
                setBoolean(6, permissions.canInteract)
                setBoolean(7, permissions.canOpenChests)
                setBoolean(8, permissions.canDamageMobs)
            }
        }
    }

    fun addClaim(player: UUID, chunk: ClaimChunk, block: (ClaimAddResult) -> Unit) {
        plugin.runTaskAsynchronously {
            val remainedClaims = getRemainedClaims(player)
            if (remainedClaims < 1) return@runTaskAsynchronously block(NotEnoughClaims)
            val claim = getClaim(chunk)
            if (claim != null) return@runTaskAsynchronously block(AlreadyClaimed(claim.ownerName))
            claims[chunk] = Claim(player, chunk.world, chunk.x, chunk.z)
            executeUpdate("""
                    INSERT INTO claim(player_uuid_most, player_uuid_least, world, x, z)
                    VALUES (?, ?, ?, ?, ?)
                """.trimIndent()) {
                setLong(1, player.mostSignificantBits)
                setLong(2, player.leastSignificantBits)
                setString(3, chunk.world)
                setInt(4, chunk.x)
                setInt(5, chunk.z)
            }
            return@runTaskAsynchronously block(Success(remainedClaims - 1))
        }
    }

    fun getRemainedClaims(player: UUID) = getMaxClaims(player) - getUsedClaims(player)

    fun getUsedClaims(player: UUID) = claims.values.count { it.owner == player }

    fun getMaxClaims(player: UUID) = maxClaims[player] ?: 0

    fun setMaxClaims(player: UUID, maxClaims: Int) {
        plugin.runTaskAsynchronously {
            this.maxClaims[player] = maxClaims
            executeUpdate("""
                    INSERT OR REPLACE INTO player_max_claims(player_uuid_most, player_uuid_least, max_claims)
                    VALUES (?, ?, ?)
                """.trimIndent()) {
                setLong(1, player.mostSignificantBits)
                setLong(2, player.leastSignificantBits)
                setInt(3, maxClaims)
            }
        }
    }

    fun removeClaim(chunk: ClaimChunk) {
        plugin.runTaskAsynchronously {
            claims.remove(chunk)
            executeUpdate("DELETE FROM claim WHERE world = ? AND x = ? AND z = ?") {
                setString(1, chunk.world)
                setInt(2, chunk.x)
                setInt(3, chunk.z)
            }
        }
    }

    fun removeClaim(claim: Claim) = removeClaim(ClaimChunk(claim.world, claim.x, claim.z))

    fun removeAllClaims(player: UUID) {
        plugin.runTaskAsynchronously {
            claims.values.removeIf { it.owner == player }
            executeUpdate("DELETE FROM claim WHERE player_uuid_most = ? AND player_uuid_least = ?") {
                setLong(1, player.mostSignificantBits)
                setLong(2, player.leastSignificantBits)
            }
        }
    }

    fun getClaim(chunk: ClaimChunk) = claims[chunk]

    fun getClaims(player: UUID): List<Claim> = claims.values.filter { it.owner == player }

    private fun <T> executeQuery(query: String, parameters: PreparedStatement.() -> Unit = {}, block: ResultSet.() -> T): T {
        return DriverManager.getConnection(url).use {
            val statement = it.prepareStatement(query)
            statement.parameters()
            statement.executeQuery().block()
        }
    }

    private fun executeUpdate(query: String, parameters: PreparedStatement.() -> Unit = {}) {
        return DriverManager.getConnection(url).use {
            val statement = it.prepareStatement(query)
            statement.parameters()
            statement.executeUpdate()
        }
    }
}

sealed class ClaimAddResult
data class Success(val remainedClaims: Int) : ClaimAddResult()
object NotEnoughClaims : ClaimAddResult()
data class AlreadyClaimed(val owner: String) : ClaimAddResult()

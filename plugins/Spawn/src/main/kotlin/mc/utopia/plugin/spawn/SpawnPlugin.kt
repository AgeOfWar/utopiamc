package mc.utopia.plugin.spawn

import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.registerEvents
import org.bukkit.Bukkit.createWorld
import org.bukkit.WorldCreator
import org.bukkit.plugin.java.JavaPlugin

class SpawnPlugin : JavaPlugin() {
    override fun onEnable() {
        val (spawnName) = fromJsonOrElse("config.json", Config())
        val spawn = createWorld(WorldCreator(spawnName))!!
        registerEvents(SpawnInteractions(spawn))
        registerEvents(PlayerTeleporter(spawn))
    }

    data class Config(val spawnName: String = "spawn")
}
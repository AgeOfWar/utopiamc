package mc.utopia.plugin.spawn

import org.bukkit.World
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockDamageEvent
import org.bukkit.event.entity.CreatureSpawnEvent
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason.*
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.player.PlayerInteractEvent

class SpawnInteractions(private val spawn: World) : Listener {
    @EventHandler
    fun onPlayerInteract(event: PlayerInteractEvent) {
        val player = event.player
        if (player.world == spawn && !player.isOp) {
            event.isCancelled = true
        }
    }

    @EventHandler
    fun onEntitySpawn(event: CreatureSpawnEvent) {
        val entity = event.entity
        if (entity.world == spawn) {
            if (event.spawnReason != CUSTOM && event.spawnReason != DEFAULT) {
                event.isCancelled = true
            }
        }
    }

    @EventHandler
    fun onEntityDamage(event: EntityDamageByEntityEvent) {
        val damager = event.damager
        if (damager.world == spawn) {
            if (damager !is Player || !damager.isOp) {
                event.isCancelled = true
            }
        }
    }

    @EventHandler
    fun onBlockDamage(event: BlockDamageEvent) {
        if (event.block.world == spawn) {
            event.isCancelled = true
        }
    }
}
package mc.utopia.plugin.spawn

import org.bukkit.World
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerRespawnEvent
import org.bukkit.event.player.PlayerTeleportEvent

class PlayerTeleporter(private val spawn: World) : Listener {
    @EventHandler
    fun onPlayerRespawn(event: PlayerRespawnEvent) {
        if (!event.isBedSpawn && !event.isAnchorSpawn) {
            event.respawnLocation = spawn.spawnLocation
        }
    }

    @EventHandler
    fun onPlayerEndExit(event: PlayerTeleportEvent) {
        if (event.cause == PlayerTeleportEvent.TeleportCause.END_PORTAL && event.from.world?.name == "world_the_end") {
            event.setTo(spawn.spawnLocation)
        }
    }

    @EventHandler
    fun onPlayerFirstJoin(event: PlayerJoinEvent) {
        val player = event.player
        if (!player.hasPlayedBefore()) {
            player.teleport(spawn.spawnLocation)
        }
    }
}
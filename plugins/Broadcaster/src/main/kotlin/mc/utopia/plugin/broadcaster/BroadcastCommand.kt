package mc.utopia.plugin.broadcaster

import mc.utopia.plugin.util.PlayerCommand
import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.sendMessage
import org.bukkit.entity.Player

class BroadcastCommand(
    private val disableBroadcastToUsers: DisableBroadcastToPlayers,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        val uuid = sender.uniqueId
        if (disableBroadcastToUsers.has(uuid)) {
            disableBroadcastToUsers.remove(uuid)
            sender.sendMessage(strings.removed)
        } else {
            disableBroadcastToUsers.add(uuid)
            sender.sendMessage(strings.added)
        }
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    data class Strings(
        val added: Text = emptyList(),
        val removed: Text = emptyList()
    )
}
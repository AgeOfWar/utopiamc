package mc.utopia.plugin.broadcaster

import mc.utopia.plugin.util.*
import org.bukkit.plugin.java.JavaPlugin

class BroadcasterPlugin : JavaPlugin() {
    override fun onEnable() {
        val (delay, period, broadcast) = fromJsonOrElse("config.json", Config())
        val messages = fromJsonOrElse("messages.json", mutableListOf<Text>())
        if (messages.isEmpty()) return
        val disableBroadcastToPlayers = DisableBroadcastToPlayers(dataFolder.resolve("disable.json"))
        var i = 0
        runTaskTimerAsynchronously(delay, period) {
            onlinePlayers.forEach {
                if (!disableBroadcastToPlayers.has(it.uniqueId)) {
                    it.sendMessage(messages[i])
                }
            }
            if (++i == messages.size) {
                i = 0
            }
        }

        registerCommand("broadcast", BroadcastCommand(disableBroadcastToPlayers, broadcast))
    }

    data class Config(
        val delay: Long = 5 * 60 * 20,
        val period: Long = 5 * 60 * 20,
        val broadcast: BroadcastCommand.Strings = BroadcastCommand.Strings()
    )
}
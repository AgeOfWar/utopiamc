package mc.utopia.plugin.broadcaster

import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import java.io.File
import java.util.*

class DisableBroadcastToPlayers(private val file: File) {
    fun get(): Set<UUID> = getMutable()

    fun has(uuid: UUID) = uuid in get()

    fun add(uuid: UUID) {
        val uuidList = getMutable()
        uuidList += uuid
        save(uuidList)
    }

    fun remove(uuid: UUID) {
        val uuidList = getMutable()
        uuidList -= uuid
        save(uuidList)
    }

    private fun save(uuidList: Set<UUID>) {
        file.parentFile.mkdirs()
        file.writer().use {
            val gson = Gson()
            gson.toJson(uuidList, it)
        }
    }

    private fun getMutable(): MutableSet<UUID> {
        if (!file.exists()) return mutableSetOf()
        return file.reader().use {
            val gson = Gson()
            gson.fromJson(it, object : TypeToken<MutableSet<UUID>>() {}.type)
        }
    }
}

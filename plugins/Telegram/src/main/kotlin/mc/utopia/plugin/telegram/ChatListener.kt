package mc.utopia.plugin.telegram

import com.vdurmont.emoji.EmojiParser
import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.methods.GetChatMember
import io.github.ageofwar.telejam.methods.SendMessage
import io.github.ageofwar.telejam.text.Text
import io.github.ageofwar.telejam.users.User
import mc.utopia.plugin.util.format
import mc.utopia.plugin.util.onlinePlayers
import mc.utopia.plugin.util.runTask
import mc.utopia.plugin.util.runTaskAsynchronously
import net.minecraft.server.v1_16_R3.AdvancementDisplay
import org.bukkit.Bukkit.getOfflinePlayer
import org.bukkit.craftbukkit.v1_16_R3.advancement.CraftAdvancement
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.event.player.AsyncPlayerChatEvent
import org.bukkit.event.player.PlayerAdvancementDoneEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.event.weather.ThunderChangeEvent
import org.bukkit.event.weather.WeatherChangeEvent
import org.bukkit.plugin.java.JavaPlugin

class ChatListener(
    private val plugin: JavaPlugin,
    private val bot: Bot,
    private val chat: Long,
    private val format: Strings,
    private val disableNotification: Boolean,
    private val telegramLinks: TelegramLinks,
    private val onlinePlayersOffset: Int
) : Listener {
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    fun onPlayerChat(event: AsyncPlayerChatEvent) {
        plugin.runTaskAsynchronously {
            val player = event.player
            val text = event.message.toTelegramText(bot, chat, telegramLinks)
            sendMessage(
                format.message.format(
                    player.format(bot, chat, telegramLinks) + mapOf("message" to text)
                )
            )
        }
    }

    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        plugin.runTaskAsynchronously {
            val player = event.player
            sendMessage(
                format.join.format(
                    mapOf("players" to (onlinePlayersOffset + onlinePlayers.size).toString()) + player.format(bot, chat, telegramLinks)
                )
            )
        }
    }

    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        plugin.runTaskAsynchronously {
            val player = event.player
            plugin.runTask {
                sendMessage(
                    format.quit.format(
                        mapOf("players" to (onlinePlayersOffset + onlinePlayers.size).toString()) + player.format(bot, chat, telegramLinks)
                    )
                )
            }
        }
    }

    @EventHandler
    fun onPlayerDeath(event: PlayerDeathEvent) {
        plugin.runTaskAsynchronously {
            val player = event.entity
            sendMessage(format.death.format(player.format(bot, chat, telegramLinks)))
        }
    }

    @EventHandler
    fun onAdvancementDone(event: PlayerAdvancementDoneEvent) {
        if (format.advancement == null) return
        val advancement = (event.advancement as? CraftAdvancement ?: return).handle.c() ?: return
        if (!advancement.i()) return
        plugin.runTaskAsynchronously {
            val player = event.player
            val name = advancement.a().text
            sendMessage(format.advancement.format(mapOf("advancement" to name) + player.format(bot, chat, telegramLinks)))
        }
    }

    @EventHandler
    fun onWeatherChange(event: WeatherChangeEvent) {
        when (event.toWeatherState()) {
            true -> {
                if (format.weather?.rain == null) return
                plugin.runTaskAsynchronously {
                    sendMessage(format.weather.rain)
                }
            }
            false -> {
                if (format.weather?.clear == null) return
                plugin.runTaskAsynchronously {
                    sendMessage(format.weather.clear)
                }
            }
        }
    }

    @EventHandler
    fun onThunderChange(event: ThunderChangeEvent) {
        when (event.toThunderState()) {
             true -> {
                if (format.weather?.thunder == null) return
                plugin.runTaskAsynchronously {
                    sendMessage(format.weather.thunder)
                }
            }
            false -> {
                if (format.weather?.noThunder == null) return
                plugin.runTaskAsynchronously {
                    sendMessage(format.weather.noThunder)
                }
            }
        }
    }

    private fun sendMessage(text: String) {
        val sendMessage = SendMessage()
            .chat(chat)
            .disableNotification(disableNotification)
            .text(Text.parseHtml(text))
        bot.execute(sendMessage)
    }

    data class Strings(
        val message: String = "\$sender: \$message",
        val join: String = "\$player joined the game, \$players online players",
        val quit: String = "\$player left the game, \$players online players",
        val death: String = "\$player died",
        val advancement: String? = null,
        val weather: Weather? = null
    ) {
        data class Weather(
            val rain: String? = null,
            val clear: String? = null,
            val thunder: String? = null,
            val noThunder: String? = null
        )
    }
}

fun escapeHtml(string: String): String = Text(string).toHtmlString()

fun Bot.getUser(chat: Long, id: Long): User {
    val getChatMember = GetChatMember()
        .chat(chat)
        .user(id)
    return execute(getChatMember).user
}

fun String.toTelegramText(bot: Bot, chat: Long, telegramLinks: TelegramLinks): String {
    return splitToSequence(' ').map {
        val i = it.indexOf('!')
        if (i > 0) {
            val name = it.substring(0, i)
            val other = it.substring(i + 1)
            val mention = @Suppress("deprecation") getOfflinePlayer(name)
            if (mention.hasPlayedBefore()) {
                val id = telegramLinks.getUserId(mention.uniqueId)
                if (id != null) {
                    if (mention.isOnline) {
                        bot.getUser(chat, id).name + other
                    } else {
                        Text.textMention(bot.getUser(chat, id)).toHtmlString() + other
                    }
                } else {
                    escapeHtml(EmojiParser.parseToUnicode(name + other))
                }
            } else {
                escapeHtml(EmojiParser.parseToUnicode(it))
            }
        } else {
            escapeHtml(EmojiParser.parseToUnicode(it))
        }
    }.joinToString(" ")
}

fun Player.format(bot: Bot, chat: Long, telegramLinks: TelegramLinks): Map<String, String> {
    val id = telegramLinks.getUserId(uniqueId)
    return if (id != null) {
        val user = bot.getUser(chat, id)
        mapOf("player" to user.name, "link" to "tg://user?id=${user.id}")
    } else {
        mapOf("player" to displayName, "link" to "")
    }
}

package mc.utopia.plugin.telegram

import mc.utopia.plugin.util.runTaskLaterAsynchronously
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.sql.DriverManager
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.*

class TelegramLinks(private val plugin: JavaPlugin, file: File) {
    private val incompleteLinks = mutableMapOf<String, UUID>()

    private val url = "jdbc:sqlite:${file.canonicalPath}"

    init {
        executeUpdate("""
            CREATE TABLE IF NOT EXISTS links (
            	uuid_most bigint NOT NULL,
                uuid_least bigint NOT NULL,
               	user_id bigint NOT NULL
            )
        """.trimIndent())
    }

    fun generateLink(uuid: UUID): String {
        val code = buildString {
            repeat(8) {
                append((('A'..'Z') + ('0'..'9')).random())
            }
        }
        incompleteLinks[code] = uuid
        plugin.runTaskLaterAsynchronously(20L * 60L * 2L) {
            incompleteLinks.remove(code)
        }
        return code
    }

    fun completeLink(code: String, id: Long): Boolean {
        val uuid = incompleteLinks.remove(code)
        return if (uuid != null) {
            put(uuid, id)
            true
        } else {
            false
        }
    }

    fun getUserId(uuid: UUID) = executeQuery("SELECT user_id FROM links WHERE uuid_most = ? AND uuid_least = ?", {
        setLong(1, uuid.mostSignificantBits)
        setLong(2, uuid.leastSignificantBits)
    }) {
        if (next()) getLong("user_id") else null
    }

    fun getUUID(userId: Long) = executeQuery("SELECT uuid_most, uuid_least FROM links WHERE user_id = ?", {
        setLong(1, userId)
    }) {
        if (next()) UUID(getLong("uuid_most"), getLong("uuid_least")) else null
    }

    private fun put(uuid: UUID, userId: Long) {
        executeUpdate("INSERT INTO links(uuid_most, uuid_least, user_id) VALUES (?, ?, ?)") {
            setLong(1, uuid.mostSignificantBits)
            setLong(2, uuid.leastSignificantBits)
            setLong(3, userId)
        }
    }

    private fun <T> executeQuery(query: String, parameters: PreparedStatement.() -> Unit = {}, block: ResultSet.() -> T): T {
        return DriverManager.getConnection(url).use {
            val statement = it.prepareStatement(query)
            statement.parameters()
            statement.executeQuery().block()
        }
    }

    private fun executeUpdate(query: String, parameters: PreparedStatement.() -> Unit = {}) {
        return DriverManager.getConnection(url).use {
            val statement = it.prepareStatement(query)
            statement.parameters()
            statement.executeUpdate()
        }
    }
}
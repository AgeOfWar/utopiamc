package mc.utopia.plugin.telegram

import io.github.ageofwar.telejam.Bot
import mc.utopia.plugin.chat.ChatPlugin
import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.registerCommand
import mc.utopia.plugin.util.registerEvents
import org.bukkit.plugin.java.JavaPlugin

class TelegramPlugin : JavaPlugin() {
    private lateinit var botThread: Thread

    override fun onEnable() {
        val chatPlugin = getPlugin(ChatPlugin::class.java)
        val (minecraftFormat, telegramFormat, telegramCommands, disableNotifications, telegram, onlinePlayersOffset) = fromJsonOrElse("config.json", Config())
        val (botToken, chat) = fromJsonOrElse("bot.json", BotConfig())
        val bot = Bot.fromToken(botToken)
        val telegramLinks = TelegramLinks(this, dataFolder.resolve("telegram.db"))
        val minecraftChatBot = MinecraftChatBot(bot, { 1000L }, chat, telegramLinks, minecraftFormat, onlinePlayersOffset, telegramCommands)
        registerEvents(ChatListener(this, bot, chat, telegramFormat, disableNotifications, telegramLinks, onlinePlayersOffset))
        registerCommand("tg", telegramCommand(this, chatPlugin, bot, chat, disableNotifications, telegramFormat.message, telegramLinks, telegram))
        botThread = Thread(minecraftChatBot).also {
            it.start()
        }
    }

    override fun onDisable() {
        botThread.interrupt()
    }

    data class Config(
        val minecraftFormat: MinecraftChatBot.MinecraftFormat = MinecraftChatBot.MinecraftFormat(),
        val telegramFormat: ChatListener.Strings = ChatListener.Strings(),
        val telegramCommands: MinecraftChatBot.Strings = MinecraftChatBot.Strings(),
        val disableNotifications: Boolean = true,
        val telegram: TelegramCommandStrings = TelegramCommandStrings(),
        val onlinePlayersOffset: Int = 0
    )

    data class BotConfig(
        val botToken: String = "",
        val chat: Long = 123456789L
    )
}
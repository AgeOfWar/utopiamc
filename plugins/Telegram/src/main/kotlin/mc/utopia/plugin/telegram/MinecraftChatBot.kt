package mc.utopia.plugin.telegram

import com.vdurmont.emoji.EmojiParser
import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.LongPollingBot
import io.github.ageofwar.telejam.chats.PrivateChat
import io.github.ageofwar.telejam.commands.Command
import io.github.ageofwar.telejam.commands.CommandHandler
import io.github.ageofwar.telejam.messages.*
import io.github.ageofwar.telejam.methods.SendMessage
import io.github.ageofwar.telejam.text.Text.parseHtml
import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.format
import mc.utopia.plugin.util.onlinePlayers
import mc.utopia.plugin.util.sendMessage
import org.bukkit.Bukkit

class MinecraftChatBot(
    bot: Bot,
    backOff: (Long) -> Long,
    chat: Long,
    telegramLinks: TelegramLinks,
    minecraftFormat: MinecraftFormat,
    onlinePlayersOffset: Int,
    strings: Strings
) : LongPollingBot(bot, backOff, Bukkit.getLogger()) {
    init {
        events.registerUpdateHandler(MessageBroadcaster(chat, minecraftFormat))
        events.registerCommand(LinkCommand(bot, telegramLinks, strings.link), "start")
        events.registerCommand(StartCommand(bot, chat, strings.help), "start")
        events.registerCommand(PlayersCommand(bot, strings.onlinePlayers, onlinePlayersOffset), "players")
    }

    data class Strings(
        val onlinePlayers: String = "There are \$players online players",
        val help: String = "",
        val link: LinkCommand.Strings = LinkCommand.Strings()
    )

    data class MinecraftFormat(
        val text: Text? = emptyList(),
        val photo: Text? = emptyList(),
        val video: Text? = emptyList(),
        val gif: Text? = emptyList(),
        val audio: Text? = emptyList()
    )
}

class MessageBroadcaster(
    private val chat: Long,
    private val minecraftFormat: MinecraftChatBot.MinecraftFormat
) : MessageHandler {
    override fun onMessage(message: Message) {
        if (message.chat.id != chat || (message is TextMessage && message.isCommand)) return
        onlinePlayers.forEach { player ->
            val format = minecraftFormat.getFormat(message)
            if (format != null) {
                player.sendMessage(format, mapOf(
                        "message" to (message.text?.let { EmojiParser.parseToAliases(it.toString()) } ?: ""),
                        "sender" to message.sender.name,
                        "id" to message.id.toString()
                ))
            }
        }
    }
}

class PlayersCommand(private val bot: Bot, private val format: String, private val onlinePlayersOffset: Int) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        val sendMessage = SendMessage()
            .replyToMessage(message)
            .text(parseHtml(format.format(mapOf("players" to (onlinePlayersOffset + onlinePlayers.size).toString()))))
        bot.execute(sendMessage)
    }
}

class StartCommand(private val bot: Bot, private val chat: Long, private val format: String) : CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        if (message.chat !is PrivateChat) return
        val sendMessage = SendMessage()
            .replyToMessage(message)
            .text(parseHtml(format.format(mapOf("chat" to chat.toString()))))
        bot.execute(sendMessage)
    }
}

class LinkCommand(
    private val bot: Bot,
    private val telegramLinks: TelegramLinks,
    private val strings: Strings
): CommandHandler {
    override fun onCommand(command: Command, message: TextMessage) {
        if (message.chat !is PrivateChat) return
        val code = command.args.toString()
        if (code.isNotEmpty()) {
            if (telegramLinks.completeLink(code, message.sender.id)) {
                val sendMessage = SendMessage()
                    .replyToMessage(message)
                    .text(parseHtml(strings.message))
                bot.execute(sendMessage)
            } else {
                val sendMessage = SendMessage()
                    .replyToMessage(message)
                    .text(parseHtml(strings.wrongCode))
                bot.execute(sendMessage)
            }
        }
    }

    data class Strings(
        val wrongCode: String = "wrong code",
        val message: String = "linked"
    )
}

private fun MinecraftChatBot.MinecraftFormat.getFormat(message: Message) = when (message) {
    is TextMessage -> text
    is PhotoMessage -> photo
    is VideoMessage, is VideoNoteMessage -> video
    is AnimationMessage -> gif
    is AudioMessage, is VoiceMessage -> audio
    else -> null
}

val Message.text: io.github.ageofwar.telejam.text.Text? get() = when (this) {
    is TextMessage -> text
    is PhotoMessage -> caption.orElse(null)
    is VideoMessage -> caption.orElse(null)
    is AnimationMessage -> caption.orElse(null)
    is AudioMessage -> caption.orElse(null)
    is VoiceMessage -> caption.orElse(null)
    else -> null
}

package mc.utopia.plugin.telegram

import io.github.ageofwar.telejam.Bot
import io.github.ageofwar.telejam.TelegramException
import io.github.ageofwar.telejam.methods.SendMessage
import io.github.ageofwar.telejam.text.Text.parseHtml
import mc.utopia.plugin.chat.ChatPlugin
import mc.utopia.plugin.util.*
import org.bukkit.entity.Player
import java.util.*

fun telegramCommand(
    plugin: TelegramPlugin,
    chatPlugin: ChatPlugin,
    bot: Bot,
    chat: Long,
    disableNotification: Boolean,
    telegramFormat: String,
    telegramLinks: TelegramLinks,
    strings: TelegramCommandStrings
): TreeCommand {
    val subCommands = mapOf(
        "reply" to TelegramReplyCommand(plugin, chatPlugin, bot, chat, disableNotification, telegramLinks, telegramFormat, strings.reply),
        "link" to TelegramLinkCommand(telegramLinks, strings.link)
    )
    return TreeCommand(subCommands, strings.usage)
}

data class TelegramCommandStrings(
    val usage: Text = emptyList(),
    val reply: TelegramReplyCommand.Strings = TelegramReplyCommand.Strings(),
    val link: TelegramLinkCommand.Strings = TelegramLinkCommand.Strings()
)

class TelegramReplyCommand(
    private val plugin: TelegramPlugin,
    private val chatPlugin: ChatPlugin,
    private val bot: Bot,
    private val chat: Long,
    private val disableNotification: Boolean,
    private val telegramLinks: TelegramLinks,
    private val telegramFormat: String,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.size < 2) {
            sender.sendMessage(strings.usage)
            return false
        }
        val replyToMessageId = args[0].toLongOrNull()
        if (replyToMessageId == null) {
            sender.sendMessage(strings.usage)
            return false
        }
        val text = StringJoiner(" ").apply {
            for (i in 1..args.lastIndex) {
                add(args[i])
            }
        }.toString()
        sendMessage(text, replyToMessageId, sender)
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    private fun sendMessage(text: String, replyToMessageId: Long, player: Player) {
        val sendMessage = SendMessage()
            .chat(chat)
            .replyToMessage(replyToMessageId)
            .disableNotification(disableNotification)
            .text(parseHtml(telegramFormat.format(
                player.format(bot, chat, telegramLinks) + mapOf("message" to escapeHtml(text.toTelegramText(bot, chat, telegramLinks)))
            )))
        plugin.runTaskAsynchronously {
            try {
                bot.execute(sendMessage)
                chatPlugin.broadcastMessage(onlinePlayers, player, text)
            } catch (e: TelegramException) {
                player.sendMessage(strings.messageNotFound)
            }
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val messageNotFound: Text = emptyList()
    )
}

class TelegramLinkCommand(
    private val telegramLinks: TelegramLinks,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        val code = telegramLinks.generateLink(sender.uniqueId)
        sender.sendMessage(strings.message, mapOf("code" to code))
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    data class Strings(
        val message: Text = emptyList()
    )
}
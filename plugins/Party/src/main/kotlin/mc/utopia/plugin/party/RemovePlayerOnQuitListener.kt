package mc.utopia.plugin.party

import mc.utopia.plugin.util.sendMessage
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerQuitEvent

class RemovePlayerOnQuitListener(
    private val parties: Parties,
    private val strings: PartyLeaveCommand.Strings
) : Listener {
    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        val player = event.player
        val party = parties.fromMember(player)
        if (party != null) {
            val wasOwner = party.owner == player
            parties.remove(party, player)
            player.sendMessage(strings.left)
            party.sendMessage(strings.playerLeft, mapOf("senderName" to player.name))
            if (wasOwner) party.sendMessage(strings.newOwner, mapOf("ownerName" to party.owner.name))
        }
    }
}
package mc.utopia.plugin.party

import org.bukkit.entity.Player

class Parties(
    private val parties: MutableMap<Player, Party> = mutableMapOf(),
    private val requests: MutableMap<Player, MutableSet<Party>> = mutableMapOf()
) {
    fun fromMember(player: Player) = parties[player]
    fun fromInvitedPlayer(player: Player) = requests[player] ?: emptySet<Party>()

    fun isRegistered(party: Party) = fromMember(party.owner) != null

    fun registerParty(party: Party): Party {
        parties[party.owner] = party
        return party
    }

    fun invite(party: Party, player: Player) {
        party.invite(player)
        requests.putIfAbsent(player, mutableSetOf())
        requests[player]!! += party
    }

    fun cancelInvite(party: Party, player: Player) {
        party.cancelInvite(player)
        requests[player]!! -= party
    }

    fun accept(party: Party, player: Player) {
        party.accept(player)
        requests[player]!! -= party
        parties[player] = party
    }

    fun remove(party: Party, player: Player) {
        party.remove(player)
        parties.remove(player)
    }
}

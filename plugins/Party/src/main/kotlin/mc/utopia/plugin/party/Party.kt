package mc.utopia.plugin.party

import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.sendMessage
import org.bukkit.entity.Player

data class Party(
    var owner: Player
) {
    val members = mutableSetOf(owner)
    val requests = mutableSetOf<Player>()

    fun invite(player: Player) {
        check(player !in members) { "$player is already a member" }
        check(player !in requests) { "$player is already invited" }
        requests += player
    }

    fun cancelInvite(player: Player) {
        check(player in requests) { "$player was not invited" }
        requests -= player
    }

    fun accept(player: Player) {
        check(player in requests) { "$player is not invited" }
        requests -= player
        members += player
    }

    fun remove(player: Player) {
        check(player in members) { "$player is not a member of the party" }
        members -= player
        if (members.isNotEmpty() && owner == player) {
            owner = members.random()
        }
    }
}

fun Party.sendMessage(format: Text, args: Map<String, String>) {
    members.forEach {
        it.sendMessage(format, args)
    }
}

fun Party.sendMessage(text: Text) {
    members.forEach {
        it.sendMessage(text)
    }
}

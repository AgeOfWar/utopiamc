package mc.utopia.plugin.party

import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.fromJsonOrElse
import mc.utopia.plugin.util.registerCommand
import mc.utopia.plugin.util.registerEvents
import org.bukkit.plugin.java.JavaPlugin

class PartyPlugin : JavaPlugin() {
    override fun onEnable() {
        val (requestExpirationTime, partyChatPrefix, format, party) = fromJsonOrElse("config.json", Config())
        val parties = Parties()
        registerEvents(ChatListener(parties, format, partyChatPrefix))
        registerEvents(RemovePlayerOnQuitListener(parties, party.leave))
        registerCommand("party", partyCommand(this, parties, requestExpirationTime, party))
    }

    data class Config(
        val requestExpirationTime: Long = 1200L,
        val partyChatPrefix: String = "!",
        val format: Text = emptyList(),
        val party: PartyCommandStrings = PartyCommandStrings()
    )
}

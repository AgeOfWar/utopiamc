package mc.utopia.plugin.party

import mc.utopia.plugin.util.Text
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent

class ChatListener(
    private val parties: Parties,
    private val format: Text,
    private val partyChatPrefix: String
) : Listener {
    @EventHandler
    fun onPlayerChat(event: AsyncPlayerChatEvent) {
        val message = event.message
        val player = event.player
        val party = parties.fromMember(player)
        if (party != null) {
            if (message.startsWith(partyChatPrefix)) {
                event.message = message.substring(partyChatPrefix.length)
            } else {
                party.sendMessage(format, mapOf("playerName" to player.name, "message" to message))
                event.isCancelled = true
            }
        }
    }
}

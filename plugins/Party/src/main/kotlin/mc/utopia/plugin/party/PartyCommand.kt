package mc.utopia.plugin.party

import mc.utopia.plugin.util.*
import org.bukkit.Bukkit
import org.bukkit.Sound
import org.bukkit.SoundCategory
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin

fun partyCommand(
    plugin: JavaPlugin,
    parties: Parties,
    requestExpirationTime: Long,
    strings: PartyCommandStrings
): TreeCommand {
    val subCommands = mapOf(
        "invite" to PartyInviteCommand(plugin, parties, requestExpirationTime, strings.invite),
        "accept" to PartyAcceptCommand(parties, strings.accept, strings.leave),
        "list" to PartyListCommand(parties, strings.list),
        "remove" to PartyRemoveCommand(parties, strings.remove, strings.leave),
        "leave" to PartyLeaveCommand(parties, strings.leave)
    )
    return TreeCommand(subCommands, strings.usage)
}

data class PartyCommandStrings(
    val usage: Text = emptyList(),
    val invite: PartyInviteCommand.Strings = PartyInviteCommand.Strings(),
    val remove: PartyRemoveCommand.Strings = PartyRemoveCommand.Strings(),
    val list: PartyListCommand.Strings = PartyListCommand.Strings(),
    val accept: PartyAcceptCommand.Strings = PartyAcceptCommand.Strings(),
    val leave: PartyLeaveCommand.Strings = PartyLeaveCommand.Strings()
)

class PartyInviteCommand(
    private val plugin: JavaPlugin,
    private val parties: Parties,
    private val requestExpirationTime: Long,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.size != 1) {
            sender.sendMessage(strings.usage)
            return false
        }
        val playerName = args[0]
        val player = Bukkit.getPlayer(playerName)
        val party = parties.fromMember(sender) ?: Party(sender)
        return when {
            party.owner != sender -> {
                sender.sendMessage(strings.noPermission)
                false
            }
            player == null -> {
                sender.sendMessage(strings.playerNotFound, mapOf("playerName" to playerName))
                false
            }
            player == sender -> {
                sender.sendMessage(strings.cannotInviteSelf)
                false
            }
            player in party.members -> {
                sender.sendMessage(strings.alreadyInParty, mapOf("playerName" to playerName))
                false
            }
            player in party.requests -> {
                sender.sendMessage(strings.alreadyRequested, mapOf("playerName" to playerName))
                false
            }
            else -> {
                if (!parties.isRegistered(party)) {
                    parties.registerParty(party)
                    sender.sendMessage(strings.partyCreated)
                }
                parties.invite(party, player)
                player.sendMessage(strings.request, mapOf("senderName" to sender.name))
                sender.sendMessage(strings.requestSent, mapOf("playerName" to playerName))
                player.playSound(player.location, Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, 1f, 1f)
                plugin.runTaskLaterAsynchronously(requestExpirationTime) {
                    if (player in party.requests) {
                        parties.cancelInvite(party, player)
                    }
                }
                true
            }
        }
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        val party = parties.fromMember(sender)
        return if (args.size == 1) {
            onlinePlayers.filter {
                it != sender && (party == null || (it !in party.members && it !in party.requests))
            }.map { it.name }
        } else {
            emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val noPermission: Text = emptyList(),
        val playerNotFound: Text = emptyList(),
        val cannotInviteSelf: Text = emptyList(),
        val alreadyInParty: Text = emptyList(),
        val alreadyRequested: Text = emptyList(),
        val partyCreated: Text = emptyList(),
        val request: Text = emptyList(),
        val requestSent: Text = emptyList()
    )
}

class PartyRemoveCommand(
    private val parties: Parties,
    private val strings: Strings,
    private val leaveStrings: PartyLeaveCommand.Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.size != 1) {
            sender.sendMessage(strings.usage)
        }
        val playerName = args[0]
        val player = Bukkit.getPlayer(playerName)
        val party = parties.fromMember(sender)
        return when {
            party == null -> {
                sender.sendMessage(strings.notInParty)
                false
            }
            party.owner != sender -> {
                sender.sendMessage(strings.noPermission)
                false
            }
            player == null -> {
                sender.sendMessage(strings.playerNotFound, mapOf("playerName" to playerName))
                false
            }
            player !in party.members && player !in party.requests -> {
                sender.sendMessage(strings.playerNotInParty, mapOf("playerName" to playerName))
                false
            }
            player in party.requests -> {
                parties.cancelInvite(party, player)
                sender.sendMessage(strings.requestCanceled, mapOf("playerName" to playerName))
                player.sendMessage(strings.inviteExpired, mapOf("senderName" to sender.name))
                true
            }
            player == sender -> {
                parties.remove(party, sender)
                sender.sendMessage(leaveStrings.left)
                party.sendMessage(leaveStrings.playerLeft, mapOf("senderName" to sender.name))
                party.sendMessage(leaveStrings.newOwner, mapOf("ownerName" to party.owner.name))
                true
            }
            else -> {
                parties.remove(party, player)
                party.sendMessage(strings.memberRemoved, mapOf("playerName" to playerName))
                player.sendMessage(strings.removed, mapOf("senderName" to sender.name))
                true
            }
        }
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        val party = parties.fromMember(sender)
        return if (args.size == 1 && party != null) {
            mutableListOf<String>().apply {
                party.members.forEach { add(it.name) }
                party.requests.forEach { add(it.name) }
            }
        } else {
            emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val notInParty: Text = emptyList(),
        val noPermission: Text = emptyList(),
        val playerNotFound: Text = emptyList(),
        val playerNotInParty: Text = emptyList(),
        val requestCanceled: Text = emptyList(),
        val inviteExpired: Text = emptyList(),
        val memberRemoved: Text = emptyList(),
        val removed: Text = emptyList()
    )
}

class PartyLeaveCommand(
    private val parties: Parties,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        val party = parties.fromMember(sender)
        return if (party == null) {
            sender.sendMessage(strings.notInParty)
            false
        } else {
            val wasOwner = party.owner == sender
            parties.remove(party, sender)
            sender.sendMessage(strings.left)
            party.sendMessage(strings.playerLeft, mapOf("senderName" to sender.name))
            if (wasOwner) party.sendMessage(strings.newOwner, mapOf("ownerName" to party.owner.name))
            true
        }
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    data class Strings(
        val usage: Text = emptyList(),
        val left: Text = emptyList(),
        val playerLeft: Text = emptyList(),
        val newOwner: Text = emptyList(),
        val notInParty: Text = emptyList()
    )
}

class PartyListCommand(
    private val parties: Parties,
    private val strings: Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        val party = parties.fromMember(sender)
        return if (party == null) {
            sender.sendMessage(strings.notInParty)
            false
        } else {
            sender.sendMessage(strings.partyInfo, mapOf(
                "ownerName" to party.owner.name,
                "members" to party.members.joinToString { it.name },
                "requests" to party.requests.joinToString { it.name }
            ))
            true
        }
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String) = emptyList<String>()

    data class Strings(
        val usage: Text = emptyList(),
        val notInParty: Text = emptyList(),
        val partyInfo: Text = emptyList()
    )
}

class PartyAcceptCommand(
    private val parties: Parties,
    private val strings: Strings,
    private val leaveStrings: PartyLeaveCommand.Strings
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (args.size != 1) {
            sender.sendMessage(strings.usage)
            return false
        }
        val ownerName = args[0]
        val owner = Bukkit.getPlayer(ownerName)
        if (owner == null) {
            sender.sendMessage(strings.playerNotFound, mapOf("ownerName" to ownerName))
            return false
        }
        val party = parties.fromMember(owner)
        if (party == null || sender !in party.requests) {
            sender.sendMessage(strings.notInvited, mapOf("ownerName" to ownerName))
            return false
        }
        val oldParty = parties.fromMember(sender)
        if (oldParty != null) {
            val wasOwner = oldParty.owner == sender
            oldParty.remove(sender)
            sender.sendMessage(leaveStrings.left)
            oldParty.sendMessage(leaveStrings.playerLeft, mapOf("senderName" to sender.name))
            if (wasOwner) party.sendMessage(leaveStrings.newOwner, mapOf("ownerName" to party.owner.name))
        }
        parties.accept(party, sender)
        sender.sendMessage(strings.accepted, mapOf("ownerName" to ownerName))
        party.sendMessage(strings.newMember, mapOf("senderName" to sender.name))
        return true
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return if (args.size == 1) {
            parties.fromInvitedPlayer(sender).toList().map { it.owner.name }
        } else {
            emptyList()
        }
    }

    data class Strings(
        val usage: Text = emptyList(),
        val playerNotFound: Text = emptyList(),
        val notInvited: Text = emptyList(),
        val accepted: Text = emptyList(),
        val newMember: Text = emptyList()
    )
}
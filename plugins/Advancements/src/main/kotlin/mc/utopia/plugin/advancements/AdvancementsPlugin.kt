package mc.utopia.plugin.advancements

import mc.utopia.plugin.claims.ClaimsPlugin
import mc.utopia.plugin.particles.ParticlesPlugin
import mc.utopia.plugin.titles.TitlesPlugin
import mc.utopia.plugin.util.*
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.NamespacedKey
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.SkullMeta
import org.bukkit.plugin.java.JavaPlugin

class AdvancementsPlugin : JavaPlugin() {
    private val claimsPlugin = getPlugin(ClaimsPlugin::class.java)
    private val titlesPlugin = getPlugin(TitlesPlugin::class.java)
    private val particlesPlugin = getPlugin(ParticlesPlugin::class.java)
    private val config = fromJsonOrElse("config.json", Config())

    override fun onEnable() {
        registerAdventureAdvancements()
        registerChallengeAdvancements()
        registerPlayTimeAdvancements()
        registerArmoredAdvancements()
        registerDiggerAdvancements()
        registerFarmerAdvancements()
        registerLumberjackAdvancements()
        registerMinerAdvancements()
        registerWarriorAdvancements()
        registerArcherAdvancements()
        registerFishermanAdvancements()
        registerSpecialistAdvancements()
    }

    private fun registerAdventureAdvancements() {
        registerAdvancement("adventure/go_to_world", oneTime = true) {
            if (config.advancementsExplanation != null) {
                it.sendMessage(config.advancementsExplanation)
            }
        }
        registerAdvancement("adventure/obtain_wood") {
            giveTitle(it, "novellino")
        }
        registerAdvancement("adventure/obtain_stone")
        registerAdvancement("adventure/obtain_iron")
        registerAdvancement("adventure/obtain_diamond")
        registerAdvancement("adventure/obtain_obsidian")
        registerAdvancement("adventure/go_to_nether") {
            giveTitle(it, "esperto")
        }
        registerAdvancement("adventure/go_to_nether_fortress")
        registerAdvancement("adventure/kill_blaze")
        registerAdvancement("adventure/go_to_stronghold")
        registerAdvancement("adventure/go_to_end") {
            giveTitle(it, "professionista")
        }
        registerKillOrAssistAdvancement("adventure/kill_ender_dragon", EntityType.ENDER_DRAGON) {
            giveTitle(it, "ammazzadraghi")
        }
        registerAdvancement("adventure/kill_wither")
        registerAdvancement("adventure/obtain_wither_skull")
        registerKillOrAssistAdvancement("adventure/kill_wither_boss", EntityType.WITHER) {
            giveTitle(it, "ammazzawither")
        }
    }

    private fun registerChallengeAdvancements() {
        registerAdvancement("challenge/adventuring_time") {
            giveTitle(it, "giramondo")
        }
        registerAdvancement("challenge/all_effects") {
            giveParticles(it, "nuvola")
        }
        registerAdvancement("challenge/all_potion_effects") {
            giveTitle(it, "caos")
        }
        registerAdvancement("challenge/balanced_diet") {
            giveParticles(it, "splendente")
        }
        registerAdvancement("challenge/bred_all_animals") {
            giveParticles(it, "innamorato")
        }
        registerAdvancement("challenge/cure_zombie_villager") {
            giveTitle(it, "dottore")
        }
        registerAdvancement("challenge/full_beacon") {
            giveTitle(it, "illuminato")
        }
        registerAdvancement("challenge/full_inventory") {
            giveTitle(it, "disordinato")
        }
        registerAdvancement("challenge/hero_of_the_village") {
            giveTitle(it, "eroe")
        }
        registerAdvancement("challenge/kill_all_hostile_mobs") {
            giveTitle(it, "cacciatore")
        }
        registerAdvancement("challenge/return_to_sender") {
            giveParticles(it, "carbonizzato")
        }
        registerAdvancement("challenge/sniper_duel") {
            giveTitle(it, "cecchino")
        }
        registerAdvancement("challenge/totem_of_undying") {
            giveTitle(it, "sopravvissuto")
        }
        registerAdvancement("challenge/trade") {
            giveTitle(it, "commerciante")
        }
        registerLevelAdvancement("challenge/level_100", 100) {
            giveParticles(it, "conoscenza")
        }
        registerAdvancement("challenge/obtain_all_discs") {
            giveParticles(it, "musica")
        }
        registerAdvancement("challenge/obtain_all_heads", oneTime = true) {
            val playerHead = ItemStack(Material.PLAYER_HEAD)
            val meta = playerHead.itemMeta as SkullMeta
            meta.owningPlayer = it
            playerHead.itemMeta = meta
            it.inventory.addItem(playerHead).forEach { (_, item) ->
                it.world.dropItem(it.location, item)
            }
        }
        registerAdvancement("challenge/obtain_elytra") {
            giveTitle(it, "angelo")
        }
        registerAdvancement("challenge/charge_respawn_anchor") {
            giveTitle(it, "infernale")
        }
        registerAdvancement("challenge/use_lodestone")
        registerAdvancement("challenge/obtain_elytra") {
            giveTitle(it, "angelo")
        }
    }

    private fun registerPlayTimeAdvancements() {
        registerPlayTimeAdvancement("time/play_1_hour", 72000) {
            giveClaimsExtension(it, 1)
        }
        registerPlayTimeAdvancement("time/play_2_hours", 2 * 72000) {
            giveClaimsExtension(it, 2)
            giveTitle(it, "anziano")
        }
        registerPlayTimeAdvancement("time/play_5_hours", 5 * 72000) {
            giveClaimsExtension(it, 3)
        }
        registerPlayTimeAdvancement("time/play_10_hours", 10 * 72000) {
            giveClaimsExtension(it, 5)
            giveParticles(it, "sabbia")
        }
        registerPlayTimeAdvancement("time/play_20_hours", 20 * 72000) {
            giveClaimsExtension(it, 10)
        }
        registerPlayTimeAdvancement("time/play_50_hours", 50 * 72000) {
            giveClaimsExtension(it, 25)
        }
        registerPlayTimeAdvancement("time/play_100_hours", 100 * 72000) {
            giveClaimsExtension(it, 50)
        }
        registerPlayTimeAdvancement("time/play_150_hours", 150 * 72000) {
            giveClaimsExtension(it, 75)
        }
        registerPlayTimeAdvancement("time/play_200_hours", 200 * 72000) {
            giveClaimsExtension(it, 100)
        }
        registerPlayTimeAdvancement("time/play_300_hours", 300 * 72000) {
            giveClaimsExtension(it, 150)
            giveTitle(it, "vip")
        }
    }

    private fun registerArmoredAdvancements() {
        registerAdvancement("armored/obtain_leather_armor")
        registerAdvancement("armored/obtain_iron_armor")
        registerAdvancement("armored/obtain_diamond_armor")
        registerAdvancement("armored/obtain_netherite_armor")
        registerAdvancement("armored/obtain_best_armor") {
            giveTitle(it, "corazzato")
        }
        registerAdvancement("armored/deflect_arrow")
    }

    private fun registerDiggerAdvancements() {
        registerAdvancement("digger/obtain_wooden_shovel")
        registerAdvancement("digger/obtain_stone_shovel")
        registerAdvancement("digger/obtain_iron_shovel")
        registerAdvancement("digger/obtain_diamond_shovel")
        registerAdvancement("digger/obtain_netherite_shovel")
        registerAdvancement("digger/obtain_best_shovel") {
            giveTitle(it, "scavatore")
        }
    }

    private fun registerFarmerAdvancements() {
        registerAdvancement("farmer/obtain_wooden_hoe")
        registerAdvancement("farmer/obtain_stone_hoe")
        registerAdvancement("farmer/obtain_iron_hoe")
        registerAdvancement("farmer/obtain_diamond_hoe")
        registerAdvancement("farmer/obtain_netherite_hoe")
        registerAdvancement("farmer/obtain_best_hoe") {
            giveTitle(it, "contadino")
        }
    }

    private fun registerLumberjackAdvancements() {
        registerAdvancement("lumberjack/obtain_wooden_axe")
        registerAdvancement("lumberjack/obtain_stone_axe")
        registerAdvancement("lumberjack/obtain_iron_axe")
        registerAdvancement("lumberjack/obtain_diamond_axe")
        registerAdvancement("lumberjack/obtain_netherite_axe")
        registerAdvancement("lumberjack/obtain_best_axe") {
            giveTitle(it, "boscaiolo")
        }
    }

    private fun registerMinerAdvancements() {
        registerAdvancement("miner/obtain_wooden_pickaxe")
        registerAdvancement("miner/obtain_stone_pickaxe")
        registerAdvancement("miner/obtain_iron_pickaxe")
        registerAdvancement("miner/obtain_diamond_pickaxe")
        registerAdvancement("miner/obtain_netherite_pickaxe")
        registerAdvancement("miner/obtain_best_pickaxe") {
            giveTitle(it, "minatore")
        }
    }

    private fun registerWarriorAdvancements() {
        registerAdvancement("warrior/obtain_wooden_sword")
        registerAdvancement("warrior/obtain_stone_sword")
        registerAdvancement("warrior/obtain_iron_sword")
        registerAdvancement("warrior/obtain_diamond_sword")
        registerAdvancement("warrior/obtain_netherite_sword")
        registerAdvancement("warrior/obtain_best_sword") {
            giveTitle(it, "guerriero")
        }
    }

    private fun registerArcherAdvancements() {
        registerAdvancement("archer/obtain_bow")
        registerAdvancement("archer/obtain_best_bow") {
            giveTitle(it, "arcere")
        }
        registerAdvancement("archer/obtain_crossbow")
        registerAdvancement("archer/crossbow_5_kill") {
            giveTitle(it, "freccia")
        }
    }

    private fun registerFishermanAdvancements() {
        registerAdvancement("fisherman/tactical_fishing")
        registerAdvancement("fisherman/obtain_fishing_rod")
        registerAdvancement("fisherman/obtain_best_fishing_rod") {
            giveTitle(it, "pescatore")
        }
        registerAdvancement("fisherman/catch_fishing_rod") {
            giveTitle(it, "fortunato")
        }
        registerAdvancement("fisherman/catch_bow") {
            giveTitle(it, "fortunato")
        }
    }

    private fun registerSpecialistAdvancements() {
        registerAdvancement("specialist/obtain_trident")
        registerAdvancement("specialist/obtain_best_trident") {
            giveTitle(it, "specialista")
        }
    }

    private fun registerAdvancement(name: String, oneTime: Boolean = false, reward: (Player) -> Unit = {}) {
        return registerEvents(Advancement(this, name, oneTime, reward))
    }

    private fun registerKillOrAssistAdvancement(name: String, entity: EntityType, oneTime: Boolean = false, reward: (Player) -> Unit = {}) {
        return registerEvents(KillOrAssistAdvancement(this, name, entity, oneTime, reward))
    }

    private fun registerPlayTimeAdvancement(name: String, tick: Int, oneTime: Boolean = false, reward: (Player) -> Unit = {}) {
        return registerEvents(PlayTimeAdvancement(this, name, tick, oneTime, reward))
    }

    private fun registerLevelAdvancement(name: String, level: Int, oneTime: Boolean = false, reward: (Player) -> Unit = {}) {
        return registerEvents(LevelAdvancement(this, name, level, oneTime, reward))
    }

    private fun giveTitle(player: Player, title: String) {
        if (!titlesPlugin.hasTitle(player, title)) {
            titlesPlugin.addTitle(player, title)
            player.sendMessage(config.rewards.title.format(mapOf("title" to title)))
            if (titlesPlugin.getTitles(player).size == 1) player.sendMessage(config.rewards.firstTitle)
        }
    }

    private fun giveParticles(player: Player, particles: String) {
        if (!particlesPlugin.hasParticle(player, particles)) {
            particlesPlugin.addParticle(player, particles)
            player.sendMessage(config.rewards.particles.format(mapOf("particles" to particles)))
            if (particlesPlugin.getParticles(player).size == 1) player.sendMessage(config.rewards.firstParticles)
        }
    }

    private fun giveClaimsExtension(player: Player, size: Int) {
        val oldSize = claimsPlugin.getClaimsMaxSize(player)
        if (oldSize < size) {
            claimsPlugin.setClaimsMaxSize(player, size)
            player.sendMessage(config.rewards.claimsExtension, mapOf("size" to size.toString()))
            if (oldSize == 0) player.sendMessage(config.rewards.firstClaimsExtension)
        }
    }

    fun awardAdvancementCriteria(player: Player, advancement: String, vararg criteria: String) {
        val progress = player.getAdvancementProgress(Bukkit.getAdvancement(NamespacedKey(this, advancement))!!)
        criteria.forEach {
            progress.awardCriteria(it)
        }
    }

    fun awardAdvancement(player: Player, advancement: String) {
        val progress = player.getAdvancementProgress(Bukkit.getAdvancement(NamespacedKey(this, advancement))!!)
        progress.remainingCriteria.forEach {
            progress.awardCriteria(it)
        }
    }

    data class Config(
        val advancementsExplanation: Text? = emptyList(),
        val rewards: Rewards = Rewards()
    ) {
        data class Rewards(
            val firstTitle: Text = emptyList(),
            val title: Text = emptyList(),
            val firstParticles: Text = emptyList(),
            val particles: Text = emptyList(),
            val firstClaimsExtension: Text = emptyList(),
            val claimsExtension: Text = emptyList()
        )
    }
}

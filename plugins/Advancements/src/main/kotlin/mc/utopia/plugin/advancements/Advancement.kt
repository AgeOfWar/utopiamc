package mc.utopia.plugin.advancements

import mc.utopia.plugin.util.runTask
import org.bukkit.Bukkit.getAdvancement
import org.bukkit.NamespacedKey
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerAdvancementDoneEvent
import org.bukkit.event.player.PlayerJoinEvent

open class Advancement(
    private val plugin: AdvancementsPlugin,
    private val name: String,
    private val oneTime: Boolean = false,
    private val reward: (Player) -> Unit = {}
) : Listener {
    @EventHandler(priority = EventPriority.HIGH)
    fun onPlayerJoinEvent(event: PlayerJoinEvent) {
        if (!oneTime) {
            val player = event.player
            val advancement = getAdvancement(NamespacedKey(plugin, name))!!
            val progress = player.getAdvancementProgress(advancement)
            if (progress.isDone) {
                plugin.runTask {
                    reward(player)
                }
            }
        }
    }

    @EventHandler
    fun onPlayerAdvancementDone(event: PlayerAdvancementDoneEvent) {
        if (event.advancement.key == NamespacedKey(plugin, name)) {
            val player = event.player
            plugin.runTask {
                reward(player)
            }
        }
    }

    fun Player.awardAdvancementCriteria(vararg criteria: String) {
        plugin.awardAdvancementCriteria(this, this@Advancement.name, *criteria)
    }

    fun Player.awardAdvancement() {
        plugin.awardAdvancement(this, this@Advancement.name)
    }
}

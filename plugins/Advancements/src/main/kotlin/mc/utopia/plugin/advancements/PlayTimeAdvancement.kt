package mc.utopia.plugin.advancements

import mc.utopia.plugin.util.runTaskAsynchronously
import mc.utopia.plugin.util.runTaskLater
import org.bukkit.Bukkit.getScheduler
import org.bukkit.Statistic
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.scheduler.BukkitTask

class PlayTimeAdvancement(
    private val plugin: AdvancementsPlugin,
    name: String,
    private val tick: Int,
    oneTime: Boolean = false,
    reward: (Player) -> Unit = {}
) : Advancement(plugin, name, oneTime, reward) {
    private val players = mutableMapOf<Player, BukkitTask>()

    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        plugin.runTaskAsynchronously {
            val player = event.player
            val tickPlayed = player.getStatistic(Statistic.PLAY_ONE_MINUTE)
            if (tickPlayed < tick) {
                players[player] = plugin.runTaskLater((tick - tickPlayed).toLong()) {
                    player.awardAdvancement()
                    players.remove(player)
                }
            } else player.awardAdvancement()
        }
    }

    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        val player = event.player
        players[player]?.cancel()
        players.remove(player)
    }
}
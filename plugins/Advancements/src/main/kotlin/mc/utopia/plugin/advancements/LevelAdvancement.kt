package mc.utopia.plugin.advancements

import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerLevelChangeEvent

class LevelAdvancement(
    plugin: AdvancementsPlugin,
    name: String,
    private val level: Int,
    oneTime: Boolean = false,
    reward: (Player) -> Unit = {}
) : Advancement(plugin, name, oneTime, reward) {
    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val player = event.player
        if (player.level >= level) {
            player.awardAdvancement()
        }
    }

    @EventHandler
    fun onLevelEntity(event: PlayerLevelChangeEvent) {
        val player = event.player
        if (event.newLevel >= level) {
            player.awardAdvancement()
        }
    }
}
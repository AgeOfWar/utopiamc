package mc.utopia.plugin.advancements

import org.bukkit.Bukkit.getPlayer
import org.bukkit.entity.Entity
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.entity.EntityDamageByEntityEvent
import org.bukkit.event.entity.EntityDeathEvent
import java.util.*

class KillOrAssistAdvancement(
    plugin: AdvancementsPlugin,
    name: String,
    private val entity: EntityType,
    oneTime: Boolean = false,
    reward: (Player) -> Unit = {}
) : Advancement(plugin, name, oneTime, reward) {
    private val entities = WeakHashMap<Entity, MutableList<UUID>>()

    @EventHandler
    fun onPlayerDamageEntity(event: EntityDamageByEntityEvent) {
        val entity = event.entity
        val damager = event.damager
        if (entity.type == this.entity && damager is Player) {
            if (entity !in entities) entities[entity] = mutableListOf()
            entities[entity]?.add(damager.uniqueId)
        }
    }

    @EventHandler
    fun onPlayerKillEntity(event: EntityDeathEvent) {
        val entity = event.entity
        if (entity.type == this.entity) {
            entities[entity]?.forEach { getPlayer(it)?.awardAdvancement() }
            entities.remove(entity)
        }
    }
}
package mc.utopia.plugin.chat

import mc.utopia.plugin.titles.TitlesPlugin
import mc.utopia.plugin.util.*
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.Bukkit.getPlayerExact
import org.bukkit.Sound
import org.bukkit.SoundCategory
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin

class ChatPlugin : JavaPlugin() {
    private val config: Config
    private val titlesPlugin = getPlugin(TitlesPlugin::class.java)

    init {
        config = fromJsonOrElse("config.json", Config())
    }

    fun broadcastMessage(recipients: Iterable<Player>, player: Player, message: String) {
        val title = titlesPlugin.titles[titlesPlugin.getCurrentTitle(player)] ?: error("cannot find title ${titlesPlugin.getCurrentTitle(player)}")
        val messageText = message.splitToSequence(' ').map {
            val i = it.indexOf('!')
            if (i > 0) {
                val name = it.substring(0, i)
                val other = it.substring(i + 1)
                val mention = getPlayerExact(name)
                if (mention != null) {
                    mention.playSound(mention.location, Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, 1f, 1f)
                    config.mention.format(mapOf("name" to mention.name)) + TextComponent(other) + TextComponent(" ")
                } else {
                    listOf(TextComponent(it), TextComponent(" "))
                }
            } else {
                listOf(TextComponent(it), TextComponent(" "))
            }
        }.flatten().toMutableList()
        messageText.removeAt(messageText.lastIndex)
        val text = config.format.message.format(mapOf("title" to title, "message" to messageText)).format(mapOf("name" to player.displayName))
        recipients.forEach {
            it.sendMessage(text)
        }
    }

    override fun onEnable() {
        registerEvents(ChatListener(this, config.format))
    }

    data class Config(
        val format: ChatListener.Strings = ChatListener.Strings(),
        val mention: Text = emptyList()
    )
}
package mc.utopia.plugin.chat

import mc.utopia.plugin.util.Text
import mc.utopia.plugin.util.format
import net.md_5.bungee.api.chat.BaseComponent
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent

class ChatListener(
    private val plugin: ChatPlugin,
    private val format: Strings
) : Listener {
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    fun onPlayerChat(event: AsyncPlayerChatEvent) {
        val player = event.player
        val message = event.message
        plugin.broadcastMessage(event.recipients, player, message)
        event.isCancelled = true
    }

    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val player = event.player
        //val title = titles[playersTitles.getCurrent(player)] ?: error("cannot find title ${playersTitles.getCurrent(player)}")
        event.joinMessage = BaseComponent.toLegacyText(*(format.join.format(mapOf("name" to player.displayName))).toTypedArray())
    }

    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        val player = event.player
        //val title = titles[playersTitles.getCurrent(player)] ?: error("cannot find title ${playersTitles.getCurrent(player)}")
        event.quitMessage = BaseComponent.toLegacyText(*(format.quit.format(mapOf("name" to player.displayName))).toTypedArray())
    }

    data class Strings(
        val message: Text = emptyList(),
        val join: Text = emptyList(),
        val quit: Text = emptyList()
    )
}

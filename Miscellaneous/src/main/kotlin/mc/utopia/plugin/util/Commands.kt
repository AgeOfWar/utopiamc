package mc.utopia.plugin.util

import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import java.util.*

interface ConsoleCommand {
    fun onCommand(sender: CommandSender, name: String, vararg args: String): Boolean
    fun onTabComplete(sender: CommandSender, name: String, vararg args: String): List<String>
}

interface PlayerCommand : ConsoleCommand {
    fun onCommand(sender: Player, name: String, vararg args: String): Boolean
    fun onTabComplete(sender: Player, name: String, vararg args: String): List<String>

    override fun onCommand(sender: CommandSender, name: String, vararg args: String): Boolean {
        return if (sender is Player) {
            onCommand(sender, name, *args)
        } else {
            sender.sendMessage("Only players can do this.")
            false
        }
    }

    override fun onTabComplete(sender: CommandSender, name: String, vararg args: String): List<String> {
        return if (sender is Player) {
            onTabComplete(sender, name, *args)
        } else {
            emptyList()
        }
    }
}

class DelayedCommand(
    private val plugin: JavaPlugin,
    private val command: ConsoleCommand,
    private val delay: Long,
    private val message: Text,
    private val senders: MutableSet<UUID> = mutableSetOf()
) : PlayerCommand {
    override fun onCommand(sender: Player, name: String, vararg args: String): Boolean {
        if (sender.uniqueId in senders) {
            sender.sendMessage(message)
            return false
        }
        return command.onCommand(sender, name, *args).also {
            if (it) {
                senders += sender.uniqueId
                plugin.runTaskLater(delay) {
                    senders -= sender.uniqueId
                }
            }
        }
    }

    override fun onTabComplete(sender: Player, name: String, vararg args: String): List<String> {
        return command.onTabComplete(sender, name, *args)
    }
}

class TreeCommand(
    private val subCommands: Map<String, ConsoleCommand>,
    private val syntax: Text
) : ConsoleCommand {
    override fun onCommand(sender: CommandSender, name: String, vararg args: String): Boolean {
        return if (args.isEmpty()) {
            sender.sendMessage(syntax)
            false
        } else {
            val subCommand = subCommands[args[0]]
            if (subCommand != null) {
                subCommand.onCommand(sender, args[0], *args.copyOfRange(1, args.size))
            } else {
                sender.sendMessage(syntax)
                false
            }
        }
    }

    override fun onTabComplete(sender: CommandSender, name: String, vararg args: String): List<String> {
        return when (args.size) {
            1 -> subCommands.mapNotNull { (name, command) -> if (command is AdminCommand && !sender.isOp) null else name }
            else -> subCommands[args[0]]?.onTabComplete(sender, args[0], *args.copyOfRange(1, args.size)) ?: emptyList()
        }
    }
}

class AdminCommand(private val command: ConsoleCommand, private val message: Text) : ConsoleCommand {
    override fun onCommand(sender: CommandSender, name: String, vararg args: String): Boolean {
        return if (sender.isOp) {
            command.onCommand(sender, name, *args)
        } else {
            sender.sendMessage(message)
            false
        }
    }

    override fun onTabComplete(sender: CommandSender, name: String, vararg args: String): List<String> {
        return if (sender.isOp) command.onTabComplete(sender, name, *args) else emptyList()
    }
}

package mc.utopia.plugin.util

import com.google.gson.reflect.TypeToken
import org.bukkit.Bukkit
import org.bukkit.Bukkit.getScheduler
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.command.TabExecutor
import org.bukkit.entity.Player
import org.bukkit.event.Listener
import org.bukkit.plugin.java.JavaPlugin

val onlinePlayers: Collection<Player> get() = Bukkit.getOnlinePlayers()

fun JavaPlugin.registerEvents(listener: Listener) {
    Bukkit.getPluginManager().registerEvents(listener, this)
}

fun JavaPlugin.registerCommand(name: String, command: ConsoleCommand) {
    val executor = object : TabExecutor {
        override fun onCommand(sender: CommandSender, cmd: Command, name: String, vararg args: String): Boolean {
            command.onCommand(sender, name, *args)
            return true
        }

        override fun onTabComplete(sender: CommandSender, cmd: Command, name: String, vararg args: String): List<String> {
            return command.onTabComplete(sender, name, *args).filter { it.startsWith(args.last(), ignoreCase = true) }
        }
    }
    getCommand(name)!!.setExecutor(executor)
}

inline fun <reified T> JavaPlugin.fromJsonOrElse(path: String, default: T): T {
    val file = dataFolder.resolve(path)
    return if (file.exists()) {
        file.reader().use { reader ->
            gson.fromJson<T>(reader, object : TypeToken<T>() {}.type)
        }
    } else {
        default.also {
            file.parentFile.mkdirs()
            file.writer().use { writer ->
                gson.toJson(it, writer)
            }
        }
    }
}

fun JavaPlugin.runTask(block: () -> Unit) = getScheduler().runTask(this, block)
fun JavaPlugin.runTaskAsynchronously(block: () -> Unit) = getScheduler().runTaskAsynchronously(this, block)
fun JavaPlugin.runTaskLater(delay: Long, block: () -> Unit) = getScheduler().runTaskLater(this, block, delay)
fun JavaPlugin.runTaskLaterAsynchronously(delay: Long, block: () -> Unit) = getScheduler().runTaskLaterAsynchronously(this, block, delay)
fun JavaPlugin.runTaskTimer(delay: Long, period: Long, block: () -> Unit) = getScheduler().runTaskTimer(this, block, delay, period)
fun JavaPlugin.runTaskTimerAsynchronously(delay: Long, period: Long, block: () -> Unit) = getScheduler().runTaskTimerAsynchronously(this, block, delay, period)

package mc.utopia.plugin.util

import net.md_5.bungee.api.chat.BaseComponent
import net.md_5.bungee.api.chat.ClickEvent
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.command.CommandSender

typealias Text = List<BaseComponent>

fun CommandSender.sendMessage(text: Text) = spigot().sendMessage(*text.toTypedArray())
fun CommandSender.sendMessage(format: Text, args: Map<String, String>) = sendMessage(format.format(args))

fun Text.format(args: Map<String, String>): Text = mutableListOf<BaseComponent>().apply {
    this@format.forEach {
        val text = it.duplicate()
        if (text is TextComponent) {
            text.text = text.text.format(args)
        }
        text.clickEvent?.let {
            text.clickEvent = ClickEvent(text.clickEvent.action, text.clickEvent.value.format(args))
        }
        add(text)
    }
}

@JvmName("formatText")
fun Text.format(args: Map<String, Text>): Text = mutableListOf<BaseComponent>().apply {
    this@format.forEach {
        val text = it.duplicate()
        if (text is TextComponent && text.text.startsWith("$")) {
            val replace = args[text.text.substring(1)]
            if (replace != null) {
                addAll(replace)
                return@forEach
            }
        }
        add(text)
    }
}

fun String.format(args: Map<String, String>): String {
    var result = this
    args.forEach { (key, value) ->
        result = result.replace("$$key", value)
    }
    return result
}
package mc.utopia.plugin.util

import com.google.gson.*
import net.md_5.bungee.api.chat.*
import net.md_5.bungee.chat.*
import org.bukkit.*
import org.bukkit.block.data.BlockData
import org.bukkit.inventory.ItemStack
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.*

val gsonBuilder: GsonBuilder
    get() = GsonBuilder().apply {
    class ListDeserializer : JsonDeserializer<List<*>> {
        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): List<*>? {
            typeOfT as ParameterizedType
            return if (json.isJsonArray) {
                json.asJsonArray.map { context.deserialize<Any?>(it, typeOfT.actualTypeArguments.single()) }
            } else {
                listOf(context.deserialize<Any?>(json, typeOfT.actualTypeArguments.single()))
            }
        }
    }
    class ItemStackSerializer : JsonSerializer<ItemStack>, JsonDeserializer<ItemStack> {
        override fun serialize(itemStack: ItemStack, typeOfT: Type, context: JsonSerializationContext): JsonElement {
            return JsonObject().apply {
                addProperty("type", itemStack.type.name.toLowerCase())
                addProperty("amount", itemStack.amount)
            }
        }

        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ItemStack {
            json as JsonObject
            return ItemStack(
                Material.valueOf(json["type"].asString.toUpperCase()),
                if (json.has("amount")) json["amount"].asInt else 1
            )
        }
    }
    class DustOptionsSerializer : JsonSerializer<Particle.DustOptions>, JsonDeserializer<Particle.DustOptions> {
        override fun serialize(dustOptions: Particle.DustOptions, typeOfT: Type, context: JsonSerializationContext): JsonElement {
            return JsonObject().apply {
                add("color", context.serialize(dustOptions))
                addProperty("size", dustOptions.size)
            }
        }

        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Particle.DustOptions {
            json as JsonObject
            return Particle.DustOptions(
                context.deserialize(json["color"], Color::class.java),
                json["size"].asFloat
            )
        }
    }
    class ColorSerializer : JsonSerializer<Color>, JsonDeserializer<Color> {
        override fun serialize(color: Color, typeOfT: Type, context: JsonSerializationContext): JsonElement {
            return JsonObject().apply {
                addProperty("red", color.red)
                addProperty("green", color.green)
                addProperty("blue", color.blue)
            }
        }

        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Color {
            json as JsonObject
            return Color.fromBGR(
                json["red"].asInt,
                json["green"].asInt,
                json["blue"].asInt
            )
        }
    }
    class BlockDataSerializer : JsonSerializer<BlockData>, JsonDeserializer<BlockData> {
        override fun serialize(blockData: BlockData, typeOfT: Type, context: JsonSerializationContext): JsonElement {
            return JsonObject().apply {
                addProperty("material", blockData.material.toString())
                addProperty("data", blockData.asString)
            }
        }

        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): BlockData {
            json as JsonObject
            return Bukkit.createBlockData(
                Material.valueOf(json["material"].asString.toUpperCase()),
                if (json.has("data")) json["data"].toString() else null
            )
        }
    }
    class LocationSerializer : JsonSerializer<Location>, JsonDeserializer<Location> {
        override fun serialize(location: Location, typeOfT: Type, context: JsonSerializationContext): JsonElement {
            return JsonObject().apply {
                location.world?.let { addProperty("world", it.name) }
                addProperty("x", location.x)
                addProperty("y", location.y)
                addProperty("z", location.z)
                addProperty("pitch", location.pitch)
                addProperty("yaw", location.yaw)
            }
        }

        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Location {
            json as JsonObject
            return Location(
                Bukkit.getWorld(json["world"].asString),
                json["x"].asDouble,
                json["y"].asDouble,
                json["z"].asDouble,
                if (json.has("yaw")) json["yaw"].asFloat else 0f,
                if (json.has("pitch")) json["pitch"].asFloat else 0f
            )
        }
    }
    class UUIDSerializer : JsonSerializer<UUID>, JsonDeserializer<UUID> {
        override fun serialize(uuid: UUID, typeOfT: Type, context: JsonSerializationContext): JsonElement {
            return JsonPrimitive(uuid.toString())
        }

        override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): UUID {
            return UUID.fromString(json.asString)
        }
    }
    registerTypeAdapter(List::class.java, ListDeserializer())
    registerTypeAdapter(ItemStack::class.java, ItemStackSerializer())
    registerTypeAdapter(Particle.DustOptions::class.java, DustOptionsSerializer())
    registerTypeAdapter(Color::class.java, ColorSerializer())
    registerTypeAdapter(BlockData::class.java, BlockDataSerializer())
    registerTypeAdapter(BaseComponent::class.java, ComponentSerializer())
    registerTypeAdapter(TextComponent::class.java, TextComponentSerializer())
    registerTypeAdapter(TranslatableComponent::class.java, TranslatableComponentSerializer())
    registerTypeAdapter(KeybindComponent::class.java, KeybindComponentSerializer())
    registerTypeAdapter(ScoreComponent::class.java, ScoreComponentSerializer())
    registerTypeAdapter(SelectorComponent::class.java, SelectorComponentSerializer())
    registerTypeAdapter(Location::class.java, LocationSerializer())
    registerTypeAdapter(UUID::class.java, UUIDSerializer())
    setPrettyPrinting()
}

val gson: Gson = gsonBuilder.create()